package tf2crates;

import cpw.mods.fml.common.FMLCommonHandler;
import net.minecraftforge.common.MinecraftForge;
import tf2crates.handler.EventHandler;
import tf2crates.handler.TooltipHandler;
import tlhpoeCore.TLHPoE;

public class ClientProxyTC extends ServerProxyTC {
	@Override
	public void initClient() {
		TLHPoE.registerUpdateDetector(ReferenceTC.ID, ReferenceTC.NAME,
				ReferenceTC.VERSION, "0B6mhkrh-GwwwWFFnNFlqWklCVFE");

		MinecraftForge.EVENT_BUS.register(new TooltipHandler());

		FMLCommonHandler.instance().bus().register(new EventHandler());
	}
}