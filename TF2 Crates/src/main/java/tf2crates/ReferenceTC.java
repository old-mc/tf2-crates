package tf2crates;

public class ReferenceTC {
	public static final String	ID						= "tf2crates";
	public static final String	NAME					= "TF2 Crates";
	public static final String	VERSION					= "1.5";

	public static String		CURRENT_SPECIAL_CRATE	= null;
}