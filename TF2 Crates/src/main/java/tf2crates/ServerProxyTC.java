package tf2crates;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import baubles.api.IBauble;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.registry.FMLControlledNamespacedRegistry;
import cpw.mods.fml.common.registry.GameData;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.BlockFence;
import net.minecraft.block.BlockStairs;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemBow;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemSlab;
import net.minecraft.item.ItemSpade;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.item.ItemTool;
import net.minecraft.potion.Potion;
import net.minecraft.util.WeightedRandomChestContent;
import net.minecraftforge.common.ChestGenHooks;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.oredict.OreDictionary;
import tf2crates.crate.Crates;
import tf2crates.crate.RandomLoot;
import tf2crates.crate.RandomRandomLoot;
import tf2crates.handler.AnvilHandler;
import tf2crates.handler.AttackHandler;
import tf2crates.handler.DeathHandler;
import tf2crates.handler.TickHandler;
import tf2crates.item.ItemCrate;
import tf2crates.item.ItemKey;
import tf2crates.item.ItemKnife;
import tf2crates.item.ItemModAxe;
import tf2crates.item.ItemModPickaxe;
import tf2crates.item.ItemModShovel;
import tf2crates.item.ItemNoiseMaker;
import tf2crates.item.ItemPaint;
import tf2crates.item.ItemSandvich;
import tf2crates.item.ItemScrap;
import tf2crates.item.ItemUnusualEffect;

public class ServerProxyTC {
	public static boolean		unbreakableSuperWeapons;
	public static int			keyRarity;
	public static boolean		sandvichEnabled;
	public static boolean		difficultKeyRecipe;
	public static int			numRandomCrates;

	public static ToolMaterial	MOD_WEAPON;

	public static Item			unusualEffect;
	public static Item			strangifier;

	public static Item			paint;
	public static Item			crate;
	public static Item			key;
	public static Item			scrap;

	public static Item			equalizer;
	public static Item			escapePlan;

	public static Item			marketGardener;

	public static Item			axtinguisher;
	public static Item			sharpenedVolcanoFragment;
	public static Item			powerJack;

	public static Item			conniversKunai;

	public static Item			sandvich;

	public static Item			noiseMakerBirthday;
	public static Item			noiseMakerSpooky;
	public static Item			noiseMakerFestive;

	public void initServer() {
		initConfig();

		MOD_WEAPON = EnumHelper.addToolMaterial("MOD_WEAPON", 2,
				unbreakableSuperWeapons ? -1 : 1561, 2F, 5F, 22);

		strangifier = new Item().setUnlocalizedName("strangifier")
				.setTextureName(ReferenceTC.ID + ":strangifier")
				.setMaxStackSize(1).setCreativeTab(tab);
		unusualEffect = new ItemUnusualEffect().setCreativeTab(tab);

		GameRegistry.registerItem(unusualEffect, "unusualEffect");
		GameRegistry.registerItem(strangifier, "strangifier");

		paint = new ItemPaint().setCreativeTab(tab);
		crate = new ItemCrate().setCreativeTab(tab);
		key = new ItemKey().setCreativeTab(tab);
		scrap = new ItemScrap().setCreativeTab(tab);

		GameRegistry.registerItem(paint, "paint");
		GameRegistry.registerItem(crate, "crate");
		GameRegistry.registerItem(key, "key");
		GameRegistry.registerItem(scrap, "scrap");

		equalizer = new ItemModPickaxe("equalizer").setCreativeTab(tab);
		escapePlan = new ItemModPickaxe("escapePlan").setCreativeTab(tab);
		marketGardener = new ItemModShovel("marketGardener", 5)
				.setCreativeTab(tab);
		axtinguisher = new ItemModAxe("axtinguisher", 6)
				.setCreativeTab(tab);
		sharpenedVolcanoFragment = new ItemModAxe(
				"sharpenedVolcanoFragment", 4).setCreativeTab(tab);
		powerJack = new ItemModAxe("powerJack").setCreativeTab(tab);
		conniversKunai = new ItemKnife("conniversKunai", 3)
				.setCreativeTab(tab);

		GameRegistry.registerItem(equalizer, "equalizer");
		GameRegistry.registerItem(escapePlan, "escapePlan");
		GameRegistry.registerItem(marketGardener, "marketGardener");
		GameRegistry.registerItem(axtinguisher, "axtinguisher");
		GameRegistry.registerItem(sharpenedVolcanoFragment,
				"sharpenedVolcanoFragment");
		GameRegistry.registerItem(powerJack, "powerJack");
		GameRegistry.registerItem(conniversKunai, "conniversKunai");

		if (sandvichEnabled) {
			sandvich = new ItemSandvich().setCreativeTab(tab);
			GameRegistry.registerItem(sandvich, "sandvich");
		}

		noiseMakerBirthday = new ItemNoiseMaker("noiseMakerBirthday",
				ReferenceTC.ID + ":tf2crates.birthday")
						.setCreativeTab(tab);
		noiseMakerSpooky = new ItemNoiseMaker("noiseMakerSpooky",
				"mob.blaze.breath", "mob.creeper.say", "mob.endermen.idle",
				"mob.endermen.stare", "mob.endermen.scream",
				"mob.ghast.charge", "mob.ghast.death", "mob.ghast.moan",
				"mob.silverfish.say", "mob.skeleton.say",
				"mob.skeleton.step", "mob.spider.say", "mob.zombie.say")
						.setCreativeTab(tab);
		noiseMakerFestive = new ItemNoiseMaker("noiseMakerFestive",
				ReferenceTC.ID + ":tf2crates.hohohohoho",
				ReferenceTC.ID + ":tf2crates.sleighbells")
						.setCreativeTab(tab);

		GameRegistry.registerItem(noiseMakerBirthday,
				"noiseMakerBirthday");
		GameRegistry.registerItem(noiseMakerSpooky, "noiseMakerSpooky");
		GameRegistry.registerItem(noiseMakerFestive, "noiseMakerFestive");

		MinecraftForge.EVENT_BUS.register(new AnvilHandler());
		MinecraftForge.EVENT_BUS.register(new DeathHandler());
		MinecraftForge.EVENT_BUS.register(new AttackHandler());

		FMLCommonHandler.instance().bus().register(new TickHandler());

		GameRegistry.addShapelessRecipe(new ItemStack(scrap, 3, 0),
				new ItemStack(Items.iron_ingot, 1),
				new ItemStack(Items.iron_ingot, 1),
				new ItemStack(Items.gold_ingot, 1));
		GameRegistry.addShapelessRecipe(new ItemStack(scrap, 1, 1),
				new ItemStack(scrap, 1, 0), new ItemStack(scrap, 1, 0),
				new ItemStack(scrap, 1, 0));
		GameRegistry.addShapelessRecipe(new ItemStack(scrap, 1, 2),
				new ItemStack(scrap, 1, 1), new ItemStack(scrap, 1, 1),
				new ItemStack(scrap, 1, 1));

		if (difficultKeyRecipe) {
			GameRegistry.addShapelessRecipe(new ItemStack(key, 1),
					new ItemStack(scrap, 1, 2), new ItemStack(scrap, 1, 2),
					new ItemStack(scrap, 1, 2),
					new ItemStack(scrap, 1, 2));
		} else {
			GameRegistry.addShapelessRecipe(new ItemStack(key, 1),
					new ItemStack(scrap, 1, 2), new ItemStack(scrap, 1, 2),
					new ItemStack(scrap, 1, 2));
		}

		GameRegistry.addShapelessRecipe(new ItemStack(strangifier, 1),
				new ItemStack(scrap, 1, 1), new ItemStack(scrap, 1, 0),
				new ItemStack(scrap, 1, 0));

		GameRegistry.addSmelting(new ItemStack(scrap, 1, 2),
				new ItemStack(scrap, 3, 1), 0);
		GameRegistry.addSmelting(new ItemStack(scrap, 1, 1),
				new ItemStack(scrap, 3, 0), 0);

		OreDictionary.registerOre("dye",
				new ItemStack(paint, 1, OreDictionary.WILDCARD_VALUE));

		String[] dyes = { "Black", "Red", "Green", "Brown", "Blue",
				"Purple", "Cyan", "LightGray", "Gray", "Pink", "Lime",
				"Yellow", "LightBlue", "Magenta", "Orange", "White" };

		for (int i = 0; i < 16; i++) {
			OreDictionary.registerOre("dye" + dyes[i],
					new ItemStack(paint, 1, i));
		}
	}

	public void postInitServer() {
		FMLControlledNamespacedRegistry<
				Item> items = GameData.getItemRegistry();

		int i;

		for (Iterator<Item> iterator = items.iterator();
				iterator.hasNext();) {
			Item item = iterator.next();

			if (item != null) {
				if (item instanceof ItemTool) {
					ItemTool tool = (ItemTool) item;

					addToolToCategory(item, tool.func_150913_i());

					if (tool instanceof ItemPickaxe) {
						RandomLoot.PICKAXES.add(tool);
					} else if (tool instanceof ItemSpade) {
						RandomLoot.SHOVELS.add(tool);
					} else if (tool instanceof ItemAxe) {
						RandomLoot.AXES.add(tool);
					}
				} else if (item instanceof ItemSword) {
					RandomLoot.SWORDS.add(item);

					try {
						addToolToCategory(item, ToolMaterial.valueOf(
								((ItemSword) item).getToolMaterialName()));
					} catch (IllegalArgumentException iaex) {
						// Do nothing. DragonAPI compatibility
					}
				} else if (item instanceof ItemBow) {
					RandomLoot.BOWS.add(item);
				} else if (item instanceof ItemHoe) {

					RandomLoot.HOES.add(item);

					try {
						addToolToCategory(item, ToolMaterial.valueOf(
								((ItemHoe) item).getToolMaterialName()));
					} catch (IllegalArgumentException iaex) {
						// Do nothing. DragonAPI compatibility
					}
				} else if (item instanceof ItemArmor) {
					ItemArmor armor = (ItemArmor) item;

					addArmorToCategory(item, armor.getArmorMaterial());

					switch (armor.armorType) {
						case 0:
							RandomLoot.HELMETS.add(armor);
							break;
						case 1:
							RandomLoot.CHESTPLATES.add(armor);
							break;
						case 2:
							RandomLoot.LEGGINGS.add(armor);
							break;
						case 3:
							RandomLoot.BOOTS.add(armor);
							break;
					}
				} else if (item instanceof ItemFood) {
					RandomLoot.FOOD.add(item);
				} else if (item instanceof ItemSlab) {
					RandomLoot.SLABS.add(new ItemStack(item, 1));
				} else if (item instanceof ItemBlock
						&& ((ItemBlock) item).field_150939_a instanceof BlockStairs) {
					RandomLoot.STAIRS.add(new ItemStack(item, 1));
				} else if (item instanceof ItemBlock
						&& ((ItemBlock) item).field_150939_a instanceof BlockFence) {
					RandomLoot.FENCES.add(new ItemStack(item, 1));
				} else if (item instanceof IBauble) {
					IBauble bauble = (IBauble) item;

					try {
						switch (bauble
								.getBaubleType(new ItemStack(item, 1))) {

							case AMULET:
								RandomLoot.AMULETS.add(item);
								break;
							case BELT:
								RandomLoot.BELTS.add(item);
								break;
							case RING:
								RandomLoot.RINGS.add(item);
								break;
							default:
								break;

						}
					} catch (NullPointerException npex) {
						// Do nothing, a bauble failed to load properly
					}
				}
			}
		}

		for (i = 0; i < Potion.potionTypes.length; i++) {
			RandomLoot.POTIONS
					.add(new ItemStack(Items.potionitem, 1, 8193 + i));
			RandomLoot.POTIONS
					.add(new ItemStack(Items.potionitem, 1, 8225 + i));
			RandomLoot.POTIONS
					.add(new ItemStack(Items.potionitem, 1, 8257 + i));

			RandomLoot.SPLASH_POTIONS
					.add(new ItemStack(Items.potionitem, 1, 16385 + i));
			RandomLoot.SPLASH_POTIONS
					.add(new ItemStack(Items.potionitem, 1, 16417 + i));
			RandomLoot.SPLASH_POTIONS
					.add(new ItemStack(Items.potionitem, 1, 16449 + i));
		}

		for (String oreName : OreDictionary.getOreNames()) {
			try {
				if (oreName.startsWith("dust")) {
					if (!oreName.startsWith("dustSmall")
							&& !oreName.startsWith("dustTiny")) {
						RandomLoot.DUSTS.add(
								OreDictionary.getOres(oreName).get(0));
					} else {

					}
				} else if (oreName.startsWith("arrow")) {
					RandomLoot.ARROWS
							.add(OreDictionary.getOres(oreName).get(0));
				} else if (oreName.startsWith("block")) {
					RandomLoot.BLOCKS
							.add(OreDictionary.getOres(oreName).get(0));
				} else if (oreName.startsWith("crate")) {
					RandomLoot.CRATES
							.add(OreDictionary.getOres(oreName).get(0));
				} else if (oreName.startsWith("crop")) {
					RandomLoot.CROPS
							.add(OreDictionary.getOres(oreName).get(0));
				} else if (oreName.startsWith("gem")) {
					RandomLoot.GEMS
							.add(OreDictionary.getOres(oreName).get(0));
				} else if (oreName.startsWith("ingot")) {
					RandomLoot.INGOTS
							.add(OreDictionary.getOres(oreName).get(0));
				} else if (oreName.startsWith("ore")) {
					RandomLoot.ORES
							.add(OreDictionary.getOres(oreName).get(0));
				} else if (oreName.startsWith("seed")) {
					RandomLoot.SEEDS
							.add(OreDictionary.getOres(oreName).get(0));
				} else if (oreName.startsWith("stone")) {
					RandomLoot.STONES
							.addAll(OreDictionary.getOres(oreName));
				} else if (oreName.startsWith("toolHead")) {
					RandomLoot.TOOL_HEADS
							.add(OreDictionary.getOres(oreName).get(0));
				}
			} catch (IndexOutOfBoundsException ioobex) {
				// Do nothing. Handle someone giving us a oredict entry
				// without anything actually in it
			}
		}

		RandomLoot.GLASS.addAll(OreDictionary.getOres("blockGlass"));
		RandomLoot.GLASS.addAll(OreDictionary.getOres("paneGlass"));

		RandomLoot.WOOL.addAll(OreDictionary.getOres("blockCloth"));

		RandomLoot.DYES.addAll(OreDictionary.getOres("dye"));

		RandomLoot.LOGS.addAll(OreDictionary.getOres("logWood"));

		RandomLoot.PLANKS.addAll(OreDictionary.getOres("plankWood"));

		RandomLoot.RECORDS.addAll(OreDictionary.getOres("record"));

		RandomLoot.SAPLINGS.addAll(OreDictionary.getOres("treeSapling"));

		RandomRandomLoot.init();

		Crates.init();

		WeightedRandomChestContent keyWRCC = new WeightedRandomChestContent(
				new ItemStack(key, 1), 1, 1, keyRarity);

		ChestGenHooks.addItem(ChestGenHooks.DUNGEON_CHEST, keyWRCC);

		ChestGenHooks.addItem(ChestGenHooks.BONUS_CHEST, keyWRCC);

		ChestGenHooks.addItem(ChestGenHooks.MINESHAFT_CORRIDOR, keyWRCC);

		ChestGenHooks.addItem(ChestGenHooks.PYRAMID_DESERT_CHEST, keyWRCC);

		ChestGenHooks.addItem(ChestGenHooks.PYRAMID_JUNGLE_CHEST, keyWRCC);

		ChestGenHooks.addItem(ChestGenHooks.VILLAGE_BLACKSMITH, keyWRCC);

		ChestGenHooks.addItem(ChestGenHooks.STRONGHOLD_CORRIDOR, keyWRCC);
		ChestGenHooks.addItem(ChestGenHooks.STRONGHOLD_CROSSING, keyWRCC);
		ChestGenHooks.addItem(ChestGenHooks.STRONGHOLD_LIBRARY, keyWRCC);
	}

	public void initClient() {
	}

	public static CreativeTabs	tab	= new CreativeTabs("tf2Crates") {
										@Override
										public Item getTabIconItem() {
											return crate;
										}
									};

	private static void addToolToCategory(Item item,
			ToolMaterial material) {
		ArrayList<Item> list = RandomLoot.MATERIAL_TOOLS.get(material);

		if (list == null) {
			list = new ArrayList<Item>();
		}

		list.add(item);

		RandomLoot.MATERIAL_TOOLS.remove(material);
		RandomLoot.MATERIAL_TOOLS.put(material, list);
	}

	private static void addArmorToCategory(Item armor,
			ArmorMaterial material) {
		ArrayList<Item> list = RandomLoot.MATERIAL_ARMOR.get(material);

		if (list == null) {
			list = new ArrayList<Item>();
		}

		list.add(armor);

		RandomLoot.MATERIAL_ARMOR.remove(material);
		RandomLoot.MATERIAL_ARMOR.put(material, list);
	}

	private static void initConfig() {
		Configuration config = new Configuration(
				new File("./config/TF2 Crates.cfg"), true);

		config.load();

		Property prop = config.get("Chances",
				"Chance of Crate from Mob Death", new int[] { 5, 100 });

		prop.comment = "default: 5 out of 100 (5% chance)";

		DeathHandler.crateChance = prop.getIntList();

		prop = config.get("Chances", "Chance of Key from Mob Death",
				new int[] { 2, 100 });

		prop.comment = "default: 2 out of 100 (2% chance)";

		DeathHandler.keyChance = prop.getIntList();

		prop = config.get("Chances", "Chance of Unusual Effect",
				new int[] { 2, 100 });

		prop.comment = "default: 2 out of 100 (2% chance)";

		ItemCrate.unusualChance = prop.getIntList();

		prop = config.get("Chances", "Chance of Rare Weapon",
				new int[] { 5, 100 });

		prop.comment = "default: 5 out of 100 (5% chance)";

		ItemCrate.weaponChance = prop.getIntList();

		prop = config.get("Chances", "Key Rarity", 25);

		prop.comment = "default: 25 | The rarity of keys generated inside of chests. The lower the rarer.";

		keyRarity = prop.getInt();

		prop = config.get("Recipe Tweaks", "Keys are Hard to Make", false);

		prop.comment = "default: false | If false, keys cost 3 refined. If true, keys cost 4 refined.";

		difficultKeyRecipe = prop.getBoolean();

		prop = config.get("Weapons",
				"Market Gardener Minimum Fall Distance", 0.75D);

		prop.comment = "default: 0.75 | This is to prevent players from just jumping to get the crit :)";

		AttackHandler.marketGardenerMinimumFall = prop.getDouble();

		prop = config.get("Unusual Effect Stuff", "Repair Hat", true);

		prop.comment = "default: true | If true, the helmet is repaired when combined with an unusual effect.";

		AnvilHandler.repairHat = prop.getBoolean();

		prop = config.get("Unusual Effect Stuff", "Make Hat Unbreakable",
				true);

		prop.comment = "default: false | If true, the helmet is made unbreakable when combined with an unusual effect.";

		AnvilHandler.makeUnbreakable = prop.getBoolean();

		prop = config.get("Weapons", "Unbreakable Rare Weapons", true);

		prop.comment = "default: true | If true, all weapons added by this mod are unbrekable.";

		unbreakableSuperWeapons = prop.getBoolean();

		prop = config.get("Anvil Recipes", "Strangifier Cost", 15);

		prop.comment = "default: 15 | The cost of adding a strangifier to an item.";

		AnvilHandler.strangeCost = prop.getInt();

		prop = config.get("Anvil Recipes", "Unusual Cost", 30);

		prop.comment = "default: 30 | The cost of adding an unusual effect to a helmet.";

		AnvilHandler.unusualCost = prop.getInt();

		prop = config.get("Weapons", "Backstab Difficulty", 0.95D);

		prop.comment = "default: 0.95D | The lower, the more difficult it is to get a backstab. 1 is very easy, and 0 is impossible.";

		ItemKnife.backstabDifficulty = prop.getDouble();

		prop = config.get("Weapons", "Enable Sandvich", true);

		prop.comment = "default: true | If true, enables the sandvich.";

		sandvichEnabled = prop.getBoolean();

		prop = config.get("Chances", "Random Crates", 64);

		prop.comment = "Number of series of crates with randomly determined loot to create";

		numRandomCrates = prop.getInt();

		config.save();
	}
}