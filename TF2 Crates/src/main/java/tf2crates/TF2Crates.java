package tf2crates;

import static tf2crates.ReferenceTC.ID;
import static tf2crates.ReferenceTC.NAME;
import static tf2crates.ReferenceTC.VERSION;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = ID, name = NAME, version = VERSION,
		dependencies = "required-after:tlhpoeCore")
public class TF2Crates { // TODO: Release
	@Instance(ID)
	public static TF2Crates		instance;

	@SidedProxy(clientSide = ID + ".ClientProxyTC",
			serverSide = ID + ".ServerProxyTC")
	public static ServerProxyTC	proxy;

	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		proxy.initServer();
		proxy.initClient();
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent event) {
		proxy.postInitServer();
	}
}