package tf2crates.crate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;
import tf2crates.ServerProxyTC;
import tlhpoeCore.ReferenceT;
import tlhpoeCore.util.MathUtil;

public class Crates {
	public static final Map<Integer,
			Object[]>							CRATE_LOOT		= new HashMap<
					Integer, Object[]>();
	public static final Map<String,
			Integer>							SPECIAL_CRATES	= new HashMap<
					String, Integer>();
	public static final Map<Integer,
			String>								SCRATES_REVERSE	= new HashMap<
					Integer, String>();

	public static void init() {
		addNormalCrates();

		addSpecialCrates();

		List<Integer> IDs = new ArrayList<Integer>();
		List<String> names = new ArrayList<String>();

		IDs.addAll(SPECIAL_CRATES.values());
		names.addAll(SPECIAL_CRATES.keySet());

		for (int i = 0; i < SPECIAL_CRATES.size(); i++) {
			SCRATES_REVERSE.put(IDs.get(i), names.get(i));
		}

		if (ReferenceT.DEOBFUSCATED) {
			for (int i = 1; i < getNumberOfSeries() + 1; i++) {
				Object[] lootItems = getLootForCrate(i);

				if (SPECIAL_CRATES.containsValue(i)) {
					System.out.println("* '''"
							+ StatCollector.translateToLocal("crate."
									+ SCRATES_REVERSE.get(i) + ".name")
							+ "''' <br />");
				} else {
					System.out.println(
							"* '''Crate Series #" + i + "''' <br />");
				}

				for (int j = 0; j < lootItems.length; j++) {
					Object loot = lootItems[j];
					String name = null;

					if (loot instanceof ItemStack) {
						name = ((ItemStack) loot).getDisplayName();
					} else if (loot instanceof RandomLoot) {
						name = ((RandomLoot) loot).getDisplayName();
					}

					System.out.println("** " + name + " <br />");
				}

				System.out
						.println("** or an Exceedingly Rare Special Item!"
								+ " <br /> <br />");
				System.out.println(" ");
			}
		}
	}

	private static void addSpecialCrates() {
		addSpecialCrateLoot("spookyCrate",
				new ItemStack(ServerProxyTC.noiseMakerSpooky, 1),
				new RandomLoot.SpecificItem(Items.emerald, 8, 16),
				new RandomLoot.SpecificItem(Items.diamond, 8, 16),
				new RandomLoot.SpecificItem(Items.gold_ingot, 16, 32),
				new RandomLoot.SpecificItem(Items.iron_ingot, 32, 64),
				new RandomLoot.SpecificItem(Items.cookie, 48, 64));

		addSpecialCrateLoot("festiveCrate",
				new ItemStack(ServerProxyTC.noiseMakerFestive, 1),
				new RandomLoot.SpecificItem(Items.emerald, 8, 16),
				new RandomLoot.SpecificItem(Items.diamond, 8, 16),
				new RandomLoot.SpecificItem(Items.gold_ingot, 16, 32),
				new RandomLoot.SpecificItem(Items.iron_ingot, 32, 64),
				new RandomLoot.SpecificItem(Items.experience_bottle, 48,
						64));

		addSpecialCrateLoot("birthdayCrate",
				new ItemStack(ServerProxyTC.noiseMakerBirthday, 1),
				new RandomLoot.SpecificItem(Items.emerald, 8, 16),
				new RandomLoot.SpecificItem(Items.diamond, 8, 16),
				new RandomLoot.SpecificItem(Items.gold_ingot, 16, 32),
				new RandomLoot.SpecificItem(Items.iron_ingot, 32, 64),
				new RandomLoot.SpecificItem(Items.cake, 16, 64));
	}

	private static void addNormalCrates() {
		addCrateLoot(new ItemStack(Items.name_tag, 1),
				new ItemStack(ServerProxyTC.paint, 1, 0),
				new ItemStack(ServerProxyTC.paint, 1, 15),
				new RandomLoot.Pickaxe(true));

		addCrateLoot(new RandomLoot.Pickaxe(true),
				new RandomLoot.Sword(true), new RandomLoot.EnchantedBook(),
				new RandomLoot.Food());

		addCrateLoot(new RandomLoot.Food(), new RandomLoot.Potion(),
				new RandomLoot.Chestplate(false),
				new RandomLoot.Boot(false));

		addCrateLoot(new ItemStack(Items.nether_wart, 32),
				new RandomLoot.SpecificItem(Items.blaze_rod, 16, 32),
				new RandomLoot.SpecificItem(Items.ghast_tear, 8, 16),
				new RandomLoot.SpecificItem(
						Item.getItemFromBlock(Blocks.enchanting_table), 2,
						4),
				new RandomLoot.SpecificItem(
						Item.getItemFromBlock(Blocks.beacon), 2, 4));

		addCrateLoot(new ItemStack(Items.name_tag, 1),
				new ItemStack(ServerProxyTC.paint, 1, 7),
				new ItemStack(ServerProxyTC.paint, 1, 8),
				new RandomLoot.Hoe(true));

		addCrateLoot(new RandomLoot.Food(), new RandomLoot.Potion(),
				new RandomLoot.Axe(false), new RandomLoot.Sword(false));

		addCrateLoot(new ItemStack(Items.name_tag, 1),
				new ItemStack(ServerProxyTC.paint, 1, 1),
				new ItemStack(ServerProxyTC.paint, 1, 14),
				new RandomLoot.Shovel(true));

		addCrateLoot(new RandomLoot.Shovel(true), new RandomLoot.Axe(true),
				new RandomLoot.EnchantedBook(), new RandomLoot.Food());

		addCrateLoot(new RandomLoot.Pickaxe(false),
				new RandomLoot.Shovel(false), new RandomLoot.Axe(false),
				new RandomLoot.Sword(false), new RandomLoot.Hoe(false));

		addCrateLoot(new ItemStack(Items.name_tag, 1),
				new ItemStack(ServerProxyTC.paint, 1, 5),
				new ItemStack(ServerProxyTC.paint, 1, 10),
				new RandomLoot.Shovel(true));

		addCrateLoot(new ItemStack(ServerProxyTC.paint, 1, 12),
				new RandomLoot.CertainMaterialArmor(ArmorMaterial.DIAMOND,
						true),
				new RandomLoot.CertainMaterialTool(ToolMaterial.EMERALD,
						true));

		addCrateLoot(new RandomLoot.Helmet(true),
				new RandomLoot.Legging(true),
				new RandomLoot.EnchantedBook(), new RandomLoot.Food());

		addCrateLoot(
				new RandomLoot.SpecificItem(Items.carrot_on_a_stick, 1, 4),
				new RandomLoot.SpecificItem(Items.bone, 32, 64),
				new RandomLoot.Food(),
				new RandomLoot.SpecificItem(Items.nether_star, 1, 4),
				new RandomLoot.Pickaxe(true));

		addCrateLoot(new ItemStack(Items.name_tag, 1),
				new ItemStack(ServerProxyTC.paint, 1, 2),
				new ItemStack(ServerProxyTC.paint, 1, 13),
				new RandomLoot.Sword(true));

		addCrateLoot(new RandomLoot.Food(), new RandomLoot.SplashPotion(),
				new RandomLoot.Shovel(false),
				new RandomLoot.Pickaxe(false));

		addCrateLoot(new ItemStack(Items.name_tag, 1),
				new ItemStack(ServerProxyTC.paint, 1, 3),
				new ItemStack(ServerProxyTC.paint, 1, 12),
				new RandomLoot.Axe(true));

		addCrateLoot(new RandomLoot.Helmet(false),
				new RandomLoot.Legging(false), new RandomLoot.Boot(false),
				new RandomLoot.Axe(false), new RandomLoot.Hoe(false));

		addCrateLoot(new ItemStack(ServerProxyTC.paint, 1, 11),
				new RandomLoot.CertainMaterialArmor(ArmorMaterial.GOLD,
						true),
				new RandomLoot.CertainMaterialTool(ToolMaterial.GOLD,
						true));

		addCrateLoot(new RandomLoot.Chestplate(true),
				new RandomLoot.Boot(true), new RandomLoot.Shovel(true),
				new RandomLoot.Axe(true));

		addCrateLoot(new RandomLoot.Hoe(true), new RandomLoot.Axe(true),
				new RandomLoot.EnchantedBook(), new RandomLoot.Food());

		addCrateLoot(new ItemStack(Items.name_tag, 1),
				new ItemStack(ServerProxyTC.paint, 1, 6),
				new ItemStack(ServerProxyTC.paint, 1, 9),
				new RandomLoot.Pickaxe(true));

		addCrateLoot(new RandomLoot.Chestplate(false),
				new RandomLoot.Pickaxe(false), new RandomLoot.Sword(true),
				new RandomLoot.Food(), new RandomLoot.Boot(true));

		addCrateLoot(new RandomLoot.Food(), new RandomLoot.SplashPotion(),
				new RandomLoot.Helmet(true), new RandomLoot.Legging(true),
				new RandomLoot.Chestplate(false));

		addCrateLoot(new RandomLoot.Chestplate(true),
				new RandomLoot.Boot(true), new RandomLoot.EnchantedBook(),
				new RandomLoot.Food());

		addCrateLoot(new ItemStack(Items.name_tag, 1),
				new ItemStack(ServerProxyTC.paint, 1, 4),
				new ItemStack(ServerProxyTC.paint, 1, 11),
				new RandomLoot.Sword(true));

		addCrateLoot(new RandomLoot.Helmet(false),
				new RandomLoot.Chestplate(false),
				new RandomLoot.Legging(false), new RandomLoot.Boot(false));

		addCrateLoot(new RandomLoot.Paint(), new RandomLoot.Potion(),
				new RandomLoot.SplashPotion(), new RandomLoot.Food());

		addCrateLoot(new ItemStack(ServerProxyTC.paint, 1, 7),
				new RandomLoot.CertainMaterialArmor(ArmorMaterial.IRON,
						true),
				new RandomLoot.CertainMaterialTool(ToolMaterial.IRON,
						true));

		addCrateLoot(new RandomLoot.Helmet(true),
				new RandomLoot.Legging(true), new RandomLoot.Pickaxe(true),
				new RandomLoot.Sword(true));

		addCrateLoot(new RandomLoot.EnchantedBook(), new RandomLoot.Food(),
				new RandomLoot.Paint(), new RandomLoot.FishingRod());

		addCrateLoot(new RandomLoot.Bow(true), new RandomLoot.Arrow(),
				new RandomLoot.FishingRod(),
				new RandomLoot.SplashPotion());

		addCrateLoot(new RandomLoot.SpecificItem(Items.dye, 16, 32, 15),
				new RandomLoot.Hoe(true), new RandomLoot.Seed(),
				new RandomLoot.Saplings());

		addCrateLoot(new RandomLoot.Amulet(), new RandomLoot.Belt(),
				new RandomLoot.Ring());

		// Add in random random crates - per MC load
		for (int i = 0; i < ServerProxyTC.numRandomCrates; i++) {
			int numInCrate = MathUtil.getRandomIntegerBetween(0, 2) + 3;

			List<RandomLoot> crateContents = new ArrayList<RandomLoot>(numInCrate);

			for (int j = 0; j < numInCrate; j++) {
				RandomLoot possibleLoot = RandomRandomLoot.getRandomLoot();

				while (crateContents.contains(possibleLoot)) {
					possibleLoot = RandomRandomLoot.getRandomLoot();
				}

				crateContents.add(possibleLoot);
			}

			CRATE_LOOT.put(CRATE_LOOT.size() + 1, crateContents.toArray());
		}
	}

	private static void addCrateLoot(Object... lootItems) {
		CRATE_LOOT.put(CRATE_LOOT.size() + 1, lootItems);
	}

	public static Object[] getLootForCrate(int series) {
		return CRATE_LOOT.get(series);
	}

	public static int getNumberOfSeries() {
		return CRATE_LOOT.size();
	}

	private static void addSpecialCrateLoot(String name,
			Object... lootItems) {
		addCrateLoot(lootItems);
		SPECIAL_CRATES.put(name, CRATE_LOOT.size());
	}

	public static Object[] getLootForSpecialCreate(String crateName) {
		return CRATE_LOOT.get(SPECIAL_CRATES.get(crateName));
	}

	public static String getNameFromSeries(int series) {
		return SCRATES_REVERSE.get(series);
	}

	public static boolean isSeriesSpecial(int series) {
		return SPECIAL_CRATES.containsValue(series);
	}

	public static int getNumberOfSpecialCrates() {
		return SPECIAL_CRATES.size();
	}

	public static int getSeriesFromName(String name) {
		return SPECIAL_CRATES.get(name);
	}
}