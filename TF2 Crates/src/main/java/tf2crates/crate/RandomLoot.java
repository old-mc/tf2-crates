package tf2crates.crate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentData;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraft.item.ItemStack;
import tf2crates.ServerProxyTC;
import tf2crates.item.ItemPaint;
import tlhpoeCore.util.MathUtil;

public abstract class RandomLoot {
	private static int	nextID	= 0;
	private int			ID;

	private List<Item>	possibleLoot;
	protected boolean	enchanted;
	public int			min		= 1, max = 1;

	protected RandomLoot(List<Item> possibleLoot, boolean enchanted) {
		this.possibleLoot = possibleLoot;
		this.enchanted = enchanted;

		ID = nextID++;
	}

	public ItemStack getLoot() {
		ItemStack loot = new ItemStack(
				possibleLoot.get(MathUtil.nextInt(possibleLoot.size())),
				MathUtil.getRandomIntegerBetween(min, max));

		if (enchanted) {
			EnchantmentHelper.addRandomEnchantment(MathUtil.getRandom(),
					loot, MathUtil.getRandomIntegerBetween(25, 30));
		}

		return loot;
	}

	public abstract String getDisplayName();

	public int getID() {
		return ID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ID;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (obj == null) {
			return false;
		}

		if (getClass() != obj.getClass()) {
			return false;
		}

		RandomLoot other = (RandomLoot) obj;

		if (ID != other.ID) {
			return false;
		}

		return true;
	}

	public static class SpecificItem extends RandomLoot {
		private ItemStack	itemStack;
		private Item		item;
		private int			meta	= -1;

		public SpecificItem(Item item, int min, int max) {
			super(null, false);

			this.itemStack = new ItemStack(item, 1);
			this.item = item;
			this.min = min;
			this.max = max;
		}

		public SpecificItem(Item item, int min, int max, int meta) {
			this(item, min, max);

			this.meta = meta;
		}

		@Override
		public String getDisplayName() {
			return min != max ? (min + " to " + max + " "
					+ itemStack.getDisplayName() + (min > 1 ? "s" : ""))
					: ((min > 1 ? (min + " ") : "A ")
							+ itemStack.getDisplayName()
							+ (min > 1 ? "s" : ""));
		}

		@Override
		public ItemStack getLoot() {
			return new ItemStack(item,
					MathUtil.getRandomIntegerBetween(min, max), meta);
		}
	}

	public static class Pickaxe extends RandomLoot {
		public Pickaxe(boolean enchanted) {
			super(PICKAXES, enchanted);
		}

		@Override
		public String getDisplayName() {
			return enchanted ? "An Enchanted Random Pickaxe"
					: "A Random Pickaxe";
		}
	}

	public static class Shovel extends RandomLoot {
		public Shovel(boolean enchanted) {
			super(SHOVELS, enchanted);
		}

		@Override
		public String getDisplayName() {
			return enchanted ? "An Enchanted Random Shovel"
					: "A Random Shovel";
		}
	}

	public static class Axe extends RandomLoot {
		public Axe(boolean enchanted) {
			super(AXES, enchanted);
		}

		@Override
		public String getDisplayName() {
			return enchanted ? "An Enchanted Random Axe" : "A Random Axe";
		}
	}

	public static class Hoe extends RandomLoot {
		public Hoe(boolean enchanted) {
			super(HOES, enchanted);
		}

		@Override
		public String getDisplayName() {
			return enchanted ? "An Enchanted Random Hoe" : "A Random Hoe";
		}
	}

	public static class ToolHead extends RandomLoot {
		public ToolHead() {
			super(null, false);
		}

		@Override
		public ItemStack getLoot() {
			ItemStack head = TOOL_HEADS
					.get(MathUtil.getRandomIntegerBetween(0,
							RandomLoot.TOOL_HEADS.size()))
					.copy();

			return head;
		}

		@Override
		public String getDisplayName() {
			return "A Random Tool Head";
		}

	}

	public static class Sword extends RandomLoot {
		public Sword(boolean enchanted) {
			super(SWORDS, enchanted);
		}

		@Override
		public String getDisplayName() {
			return enchanted ? "An Enchanted Random Sword"
					: "A Random Sword";
		}
	}

	public static class Bow extends RandomLoot {
		public Bow(boolean enchanted) {
			super(BOWS, enchanted);

			this.min = 1;
			this.max = 1;
		}

		@Override
		public String getDisplayName() {
			return enchanted ? "An Enchanted Random Bow" : "A Random Bow";
		}
	}

	public static class Seed extends RandomLoot {
		public Seed() {
			super(null, false);
		}

		@Override
		public ItemStack getLoot() {
			ItemStack seedType = SEEDS
					.get(MathUtil.getRandomIntegerBetween(0, SEEDS.size()))
					.copy();

			seedType.stackSize = MathUtil.getRandomIntegerBetween(16, 32);

			return seedType;
		}

		@Override
		public String getDisplayName() {
			return "16 to 32 Seeds of a Random Type";
		}
	}

	public static class Saplings extends RandomLoot {
		public Saplings() {
			super(null, false);
		}

		@Override
		public ItemStack getLoot() {
			ItemStack saplingType = SAPLINGS.get(
					MathUtil.getRandomIntegerBetween(0, SAPLINGS.size()))
					.copy();

			saplingType.stackSize = MathUtil.getRandomIntegerBetween(16,
					32);

			return saplingType;
		}

		@Override
		public String getDisplayName() {
			return "16 to 32 Saplings of a Random Type";
		}
	}

	public static class Arrow extends RandomLoot {
		public Arrow() {
			super(null, false);
		}

		@Override
		public ItemStack getLoot() {
			ItemStack arrowType = ARROWS.get(
					MathUtil.getRandomIntegerBetween(0, ARROWS.size()))
					.copy();

			arrowType.stackSize = MathUtil.getRandomIntegerBetween(16, 32);

			return arrowType;
		}

		@Override
		public String getDisplayName() {
			return "16 to 32 Arrows of a Random Type";
		}
	}

	public static class Helmet extends RandomLoot {
		public Helmet(boolean enchanted) {
			super(HELMETS, enchanted);
		}

		@Override
		public String getDisplayName() {
			return enchanted ? "An Enchanted Random Helmet"
					: "A Random Helmet";
		}
	}

	public static class Chestplate extends RandomLoot {
		public Chestplate(boolean enchanted) {
			super(CHESTPLATES, enchanted);
		}

		@Override
		public String getDisplayName() {
			return enchanted ? "An Enchanted Random Chestplate"
					: "A Random Chestplate";
		}
	}

	public static class Legging extends RandomLoot {
		public Legging(boolean enchanted) {
			super(LEGGINGS, enchanted);
		}

		@Override
		public String getDisplayName() {
			return enchanted ? "Enchanted Random Leggings"
					: "Random Leggings";
		}
	}

	public static class Boot extends RandomLoot {
		public Boot(boolean enchanted) {
			super(BOOTS, enchanted);
		}

		@Override
		public String getDisplayName() {
			return enchanted ? "Enchanted Random Boots" : "Random Boots";
		}
	}

	public static class Food extends RandomLoot {
		public Food() {
			super(FOOD, false);

			this.min = 16;
			this.max = 64;
		}

		@Override
		public String getDisplayName() {
			return "16 to 64 of a Random Food Item";
		}
	}

	public static class Potion extends RandomLoot {
		public Potion() {
			super(null, false);
		}

		@Override
		public String getDisplayName() {
			return "4 to 16 of a Random Potion";
		}

		@Override
		public ItemStack getLoot() {
			ItemStack loot = POTIONS.get(MathUtil.nextInt(POTIONS.size()))
					.copy();

			loot.stackSize = MathUtil.getRandomIntegerBetween(4, 16);

			return loot;
		}
	}

	public static class SplashPotion extends RandomLoot {
		public SplashPotion() {
			super(null, false);
		}

		@Override
		public String getDisplayName() {
			return "4 to 16 of a Random Splash Potion";
		}

		@Override
		public ItemStack getLoot() {
			ItemStack loot = SPLASH_POTIONS
					.get(MathUtil.nextInt(SPLASH_POTIONS.size())).copy();

			loot.stackSize = MathUtil.getRandomIntegerBetween(4, 16);

			return loot;
		}
	}

	public static class CertainMaterialTool extends RandomLoot {
		private ToolMaterial material;

		public CertainMaterialTool(ToolMaterial material,
				boolean enchanted) {
			super(MATERIAL_TOOLS.get(material), enchanted);

			this.material = material;
		}

		@Override
		public String getDisplayName() {
			return enchanted
					? "Enchanted Random " + material.name() + " Tool"
					: "Random " + material.name() + " Tool";
		}
	}

	public static class CertainMaterialArmor extends RandomLoot {
		private ArmorMaterial material;

		public CertainMaterialArmor(ArmorMaterial material,
				boolean enchanted) {
			super(MATERIAL_ARMOR.get(material), enchanted);

			this.material = material;
		}

		@Override
		public String getDisplayName() {
			return enchanted
					? "Enchanted Random " + material.name() + " Armor"
					: "Random " + material.name() + " Armor";
		}
	}

	public static class Paint extends RandomLoot {
		public Paint() {
			super(null, false);
		}

		@Override
		public ItemStack getLoot() {
			int dmg = MathUtil.nextInt(ItemPaint.TYPES.length);

			if (MathUtil.getChance(1, 50)) {
				dmg = 11;
			}

			return new ItemStack(ServerProxyTC.paint, 1, dmg);
		}

		@Override
		public String getDisplayName() {
			return "A Random Paint";
		}
	}

	public static class EnchantedBook extends RandomLoot {
		public EnchantedBook() {
			super(null, true);
		}

		@Override
		public ItemStack getLoot() {
			ItemStack loot = new ItemStack(Items.enchanted_book,
					MathUtil.getRandomIntegerBetween(2, 8));
			Enchantment enchantment = Enchantment.enchantmentsBookList[MathUtil
					.nextInt(Enchantment.enchantmentsBookList.length)];

			Items.enchanted_book.addEnchantment(loot,
					new EnchantmentData(enchantment,
							MathUtil.getRandomIntegerBetween(
									enchantment.getMinLevel(),
									enchantment.getMaxLevel())));

			return loot;
		}

		@Override
		public String getDisplayName() {
			return "2 to 8 Random Enchanted Books";
		}
	}

	public static class FishingRod extends RandomLoot {
		public FishingRod() {
			super(null, true);
		}

		@Override
		public ItemStack getLoot() {
			ItemStack loot = new ItemStack(Items.fishing_rod, 1);

			EnchantmentHelper.addRandomEnchantment(MathUtil.getRandom(),
					loot, 30);

			return loot;
		}

		@Override
		public String getDisplayName() {
			return "A Randomly Enchanted Fishing Rod";
		}
	}

	public static class Slab extends RandomLoot {
		public Slab() {
			super(null, false);
		}

		@Override
		public ItemStack getLoot() {
			ItemStack slabType = SLABS
					.get(MathUtil.getRandomIntegerBetween(0, SLABS.size()))
					.copy();

			slabType.stackSize = MathUtil.getRandomIntegerBetween(16, 64);

			return slabType;
		}

		@Override
		public String getDisplayName() {
			return "16 to 64 Slabs of a Random Type";
		}
	}

	public static class Stair extends RandomLoot {
		public Stair() {
			super(null, false);
		}

		@Override
		public ItemStack getLoot() {
			ItemStack stairType = STAIRS.get(
					MathUtil.getRandomIntegerBetween(0, STAIRS.size()))
					.copy();

			stairType.stackSize = MathUtil.getRandomIntegerBetween(16, 64);

			return stairType;
		}

		@Override
		public String getDisplayName() {
			return "16 to 64 Stairs of a Random Type";
		}
	}

	public static class Fence extends RandomLoot {
		public Fence() {
			super(null, false);
		}

		@Override
		public ItemStack getLoot() {
			ItemStack fenceType = FENCES.get(
					MathUtil.getRandomIntegerBetween(0, FENCES.size()))
					.copy();

			fenceType.stackSize = MathUtil.getRandomIntegerBetween(16, 64);

			return fenceType;
		}

		@Override
		public String getDisplayName() {
			return "16 to 64 Fences of a Random Type";
		}
	}

	public static class Planks extends RandomLoot {
		public Planks() {
			super(null, false);
		}

		@Override
		public ItemStack getLoot() {
			ItemStack plankType = PLANKS.get(
					MathUtil.getRandomIntegerBetween(0, PLANKS.size()))
					.copy();

			plankType.stackSize = MathUtil.getRandomIntegerBetween(16, 64);

			return plankType;
		}

		@Override
		public String getDisplayName() {
			return "16 to 64 Planks of a Random Type";
		}
	}

	public static class Logs extends RandomLoot {
		public Logs() {
			super(null, false);
		}

		@Override
		public ItemStack getLoot() {
			ItemStack logType = LOGS
					.get(MathUtil.getRandomIntegerBetween(0, LOGS.size()))
					.copy();

			logType.stackSize = MathUtil.getRandomIntegerBetween(16, 32);

			return logType;
		}

		@Override
		public String getDisplayName() {
			return "16 to 32 Logs of a Random Type";
		}
	}

	public static class Crop extends RandomLoot {
		public Crop() {
			super(null, false);
		}

		@Override
		public ItemStack getLoot() {
			ItemStack cropType = CROPS
					.get(MathUtil.getRandomIntegerBetween(0, CROPS.size()))
					.copy();

			cropType.stackSize = MathUtil.getRandomIntegerBetween(16, 32);

			return cropType;
		}

		@Override
		public String getDisplayName() {
			return "8 to 16 Crops of a Random Type";
		}
	}

	public static class Dye extends RandomLoot {
		public Dye() {
			super(null, false);
		}

		@Override
		public ItemStack getLoot() {
			ItemStack dyeType = DYES
					.get(MathUtil.getRandomIntegerBetween(0, DYES.size()))
					.copy();

			dyeType.stackSize = MathUtil.getRandomIntegerBetween(16, 32);

			return dyeType;
		}

		@Override
		public String getDisplayName() {
			return "16 to 32 Logs of a Random Type";
		}
	}

	public static class Record extends RandomLoot {
		public Record() {
			super(null, false);
		}

		@Override
		public ItemStack getLoot() {
			ItemStack dyeType = RECORDS.get(
					MathUtil.getRandomIntegerBetween(0, RECORDS.size()))
					.copy();

			dyeType.stackSize = 1;

			return dyeType;
		}

		@Override
		public String getDisplayName() {
			return "A Random Record";
		}
	}

	public static class Glass extends RandomLoot {
		public Glass() {
			super(null, false);
		}

		@Override
		public ItemStack getLoot() {
			ItemStack glassType = GLASS.get(MathUtil
					.getRandomIntegerBetween(0, RandomLoot.GLASS.size()))
					.copy();

			glassType.stackSize = MathUtil.getRandomIntegerBetween(32, 64);

			return glassType;
		}

		@Override
		public String getDisplayName() {
			return "32 to 64 Glass of a Random Type";
		}
	}

	public static class Wool extends RandomLoot {
		public Wool() {
			super(null, false);
		}

		@Override
		public ItemStack getLoot() {
			ItemStack woolType = WOOL.get(MathUtil
					.getRandomIntegerBetween(0, RandomLoot.WOOL.size()))
					.copy();

			woolType.stackSize = MathUtil.getRandomIntegerBetween(8, 32);

			return woolType;
		}

		@Override
		public String getDisplayName() {
			return "8 to 32 Wool of a Random Type";
		}
	}

	public static class Stone extends RandomLoot {
		public Stone() {
			super(null, false);
		}

		@Override
		public ItemStack getLoot() {
			ItemStack stoneType = STONES.get(MathUtil
					.getRandomIntegerBetween(0, RandomLoot.STONES.size()))
					.copy();

			stoneType.stackSize = MathUtil.getRandomIntegerBetween(16, 48);

			return stoneType;
		}

		@Override
		public String getDisplayName() {
			return "16 to 48 Stone of a Random Type";
		}
	}

	public static class Dust extends RandomLoot {
		public Dust() {
			super(null, false);
		}

		@Override
		public ItemStack getLoot() {
			ItemStack dustType = RandomLoot.DUSTS.get(MathUtil
					.getRandomIntegerBetween(0, RandomLoot.DUSTS.size()))
					.copy();

			dustType.stackSize = MathUtil.getRandomIntegerBetween(8, 16);

			return dustType;
		}

		@Override
		public String getDisplayName() {
			return "8 to 16 Dust of a Random Type";
		}
	}

	public static class Block extends RandomLoot {
		public Block() {
			super(null, false);
		}

		@Override
		public ItemStack getLoot() {
			ItemStack blockType = RandomLoot.DUSTS.get(MathUtil
					.getRandomIntegerBetween(0, RandomLoot.DUSTS.size()))
					.copy();

			blockType.stackSize = MathUtil.getRandomIntegerBetween(4, 8);

			return blockType;
		}

		@Override
		public String getDisplayName() {
			return "4 to 8 Random Blocks";
		}
	}

	public static class Crate extends RandomLoot {
		public Crate() {
			super(null, false);
		}

		@Override
		public ItemStack getLoot() {
			ItemStack crateType = RandomLoot.CRATES.get(MathUtil
					.getRandomIntegerBetween(0, RandomLoot.CRATES.size()))
					.copy();

			crateType.stackSize = MathUtil.getRandomIntegerBetween(4, 8);

			return crateType;
		}

		@Override
		public String getDisplayName() {
			return "4 to 8 Storage Crates of a Random Type";
		}
	}

	public static class Gem extends RandomLoot {
		public Gem() {
			super(null, false);
		}

		@Override
		public ItemStack getLoot() {
			ItemStack gemType = RandomLoot.GEMS.get(MathUtil
					.getRandomIntegerBetween(0, RandomLoot.GEMS.size()))
					.copy();

			gemType.stackSize = MathUtil.getRandomIntegerBetween(8, 16);

			return gemType;
		}

		@Override
		public String getDisplayName() {
			return "8 to 16 Gems of a Random Type";
		}
	}

	public static class Ingot extends RandomLoot {
		public Ingot() {
			super(null, false);
		}

		@Override
		public ItemStack getLoot() {
			ItemStack ingotType = RandomLoot.INGOTS.get(MathUtil
					.getRandomIntegerBetween(0, RandomLoot.INGOTS.size()))
					.copy();

			ingotType.stackSize = MathUtil.getRandomIntegerBetween(16, 32);

			return ingotType;
		}

		@Override
		public String getDisplayName() {
			return "16 to 32 Ingots of a Random Type";
		}
	}

	public static class Ore extends RandomLoot {
		public Ore() {
			super(null, false);
		}

		@Override
		public ItemStack getLoot() {
			ItemStack oreType = RandomLoot.ORES.get(MathUtil
					.getRandomIntegerBetween(0, RandomLoot.ORES.size()))
					.copy();

			oreType.stackSize = MathUtil.getRandomIntegerBetween(16, 32);

			return oreType;
		}

		@Override
		public String getDisplayName() {
			return "16 to 32 Ores of a Random Type";
		}
	}

	public static class Belt extends RandomLoot {
		public Belt() {
			super(RandomLoot.BELTS, false);
		}

		@Override
		public String getDisplayName() {
			return "A Random Belt";
		}

	}

	public static class Amulet extends RandomLoot {
		public Amulet() {
			super(RandomLoot.AMULETS, false);
		}

		@Override
		public String getDisplayName() {
			return "A Random Amulet";
		}

	}

	public static class Ring extends RandomLoot {
		public Ring() {
			super(RandomLoot.RINGS, false);
		}

		@Override
		public String getDisplayName() {
			return "A Random Ring";
		}
	}

	/*
	 * Tool Loot
	 */
	public static final List<
			Item>											PICKAXES		= new ArrayList<
					Item>();
	public static final List<
			Item>											SHOVELS			= new ArrayList<
					Item>();
	public static final List<
			Item>											AXES			= new ArrayList<
					Item>();
	public static final List<
			Item>											HOES			= new ArrayList<
					Item>();
	public static final List<
			ItemStack>										TOOL_HEADS		= new ArrayList<
					ItemStack>();

	/*
	 * Weapon Loot
	 */
	public static final List<
			Item>											SWORDS			= new ArrayList<
					Item>();
	public static final List<
			Item>											BOWS			= new ArrayList<
					Item>();
	public static final List<
			ItemStack>										ARROWS			= new ArrayList<
					ItemStack>();

	/*
	 * Tool Material Loot
	 */
	public static final Map<ToolMaterial,
			ArrayList<
					Item>>									MATERIAL_TOOLS	= new HashMap<
							ToolMaterial, ArrayList<Item>>();

	/*
	 * Armor Loot
	 */
	public static final List<
			Item>											HELMETS			= new ArrayList<
					Item>();
	public static final List<
			Item>											CHESTPLATES		= new ArrayList<
					Item>();
	public static final List<
			Item>											LEGGINGS		= new ArrayList<
					Item>();
	public static final List<
			Item>											BOOTS			= new ArrayList<
					Item>();

	/*
	 * Armor Material Loot
	 */
	public static final Map<ArmorMaterial,
			ArrayList<
					Item>>									MATERIAL_ARMOR	= new HashMap<
							ArmorMaterial, ArrayList<Item>>();

	/*
	 * Food Loot
	 */
	public static final List<
			Item>											FOOD			= new ArrayList<
					Item>();
	public static final List<
			ItemStack>										CROPS			= new ArrayList<
					ItemStack>();
	public static final List<
			ItemStack>										SEEDS			= new ArrayList<
					ItemStack>();

	/*
	 * Potion Loot
	 */
	public static final List<
			ItemStack>										POTIONS			= new ArrayList<
					ItemStack>();
	public static final List<
			ItemStack>										SPLASH_POTIONS	= new ArrayList<
					ItemStack>();

	/*
	 * Resource Loot
	 */
	public static final List<
			ItemStack>										DUSTS			= new ArrayList<
					ItemStack>();
	public static final List<
			ItemStack>										BLOCKS			= new ArrayList<
					ItemStack>();
	public static final List<
			ItemStack>										CRATES			= new ArrayList<
					ItemStack>();
	public static final List<
			ItemStack>										GEMS			= new ArrayList<
					ItemStack>();
	public static final List<
			ItemStack>										INGOTS			= new ArrayList<
					ItemStack>();
	public static final List<
			ItemStack>										ORES			= new ArrayList<
					ItemStack>();

	/*
	 * Decorative Loot
	 */
	public static final List<
			ItemStack>										GLASS			= new ArrayList<
					ItemStack>();
	public static final List<
			ItemStack>										WOOL			= new ArrayList<
					ItemStack>();
	public static final List<
			ItemStack>										DYES			= new ArrayList<
					ItemStack>();
	public static final List<
			ItemStack>										FENCES			= new ArrayList<
					ItemStack>();
	public static final List<
			ItemStack>										LOGS			= new ArrayList<
					ItemStack>();
	public static final List<
			ItemStack>										PLANKS			= new ArrayList<
					ItemStack>();
	public static final List<
			ItemStack>										RECORDS			= new ArrayList<
					ItemStack>();
	public static final List<
			ItemStack>										SLABS			= new ArrayList<
					ItemStack>();
	public static final List<
			ItemStack>										STAIRS			= new ArrayList<
					ItemStack>();
	public static final List<
			ItemStack>										STONES			= new ArrayList<
					ItemStack>();
	public static final List<
			ItemStack>										SAPLINGS		= new ArrayList<
					ItemStack>();

	/*
	 * List of weapons
	 */
	public static final List<
			Item>											WEAPONS			= new ArrayList<
					Item>();

	/*
	 * List of baubles
	 */
	public static final List<
			Item>											BELTS			= new ArrayList<
					Item>();
	public static final List<
			Item>											AMULETS			= new ArrayList<
					Item>();
	public static final List<
			Item>											RINGS			= new ArrayList<
					Item>();
}