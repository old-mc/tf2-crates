package tf2crates.crate;

import java.util.ArrayList;
import java.util.List;

import tconstruct.library.crafting.ToolBuilder;
import tconstruct.library.crafting.ToolRecipe;
import tlhpoeCore.util.MathUtil;

/**
 * Get random instances of random loot :)
 * 
 * @author ben
 *
 */
public class RandomRandomLoot {
	private static List<RandomLoot> loot;

	public static void init() {
		loot = new ArrayList<RandomLoot>();

		// Tool loot
		addToolLoot();

		// Weapon loot
		addWeaponLoot();

		// Armor loot
		addArmorLoot();

		// Potion loot
		addPotionLoot();

		// Plant loot
		addPlantLoot();
		addPlantLoot();

		// Dye loot
		addDyeLoot();
		addDyeLoot();

		// Decorative loot
		addDecorativeLoot();
		addDecorativeLoot();

		// Valuble loot
		addValubleLoot();

		// Bauble loot
		addBaubleLoot();

		// Misc Loot
		addMiscLoot();
		addMiscLoot();

		addTinkersLoot();
	}

	private static void addTinkersLoot() {
		List<ToolRecipe> combos = ToolBuilder.instance.combos;
		for (ToolRecipe toolRecipe : combos) {
			boolean isLegendary;

			if (MathUtil.nextDouble() > .8) {
				isLegendary = true;
			} else {
				isLegendary = false;
			}

			loot.add(new RandomTinkersTool(isLegendary, toolRecipe));
		}
	}

	private static void addToolLoot() {
		loot.add(new RandomLoot.Pickaxe(false));
		loot.add(new RandomLoot.Pickaxe(true));
		loot.add(new RandomLoot.Shovel(false));
		loot.add(new RandomLoot.Shovel(true));
		loot.add(new RandomLoot.Axe(false));
		loot.add(new RandomLoot.Axe(true));
		loot.add(new RandomLoot.Hoe(false));
		loot.add(new RandomLoot.Hoe(true));

		loot.add(new RandomLoot.ToolHead());
	}

	private static void addWeaponLoot() {
		loot.add(new RandomLoot.Sword(false));
		loot.add(new RandomLoot.Sword(true));
		loot.add(new RandomLoot.Bow(false));
		loot.add(new RandomLoot.Bow(true));
		loot.add(new RandomLoot.Arrow());
	}

	private static void addArmorLoot() {
		loot.add(new RandomLoot.Helmet(false));
		loot.add(new RandomLoot.Helmet(true));
		loot.add(new RandomLoot.Chestplate(false));
		loot.add(new RandomLoot.Chestplate(true));
		loot.add(new RandomLoot.Legging(false));
		loot.add(new RandomLoot.Legging(true));
		loot.add(new RandomLoot.Boot(false));
		loot.add(new RandomLoot.Boot(true));
	}

	private static void addPotionLoot() {
		loot.add(new RandomLoot.Potion());
		loot.add(new RandomLoot.SplashPotion());
	}

	private static void addPlantLoot() {
		loot.add(new RandomLoot.Food());
		loot.add(new RandomLoot.Seed());
		loot.add(new RandomLoot.Crop());
		loot.add(new RandomLoot.Saplings());
		loot.add(new RandomLoot.Logs());
		loot.add(new RandomLoot.Planks());
	}

	private static void addDyeLoot() {
		loot.add(new RandomLoot.Paint());
		loot.add(new RandomLoot.Dye());
	}

	private static void addDecorativeLoot() {
		loot.add(new RandomLoot.Slab());
		loot.add(new RandomLoot.Stair());
		loot.add(new RandomLoot.Fence());
		loot.add(new RandomLoot.Glass());
		loot.add(new RandomLoot.Stone());
	}

	private static void addValubleLoot() {
		loot.add(new RandomLoot.Dust());
		loot.add(new RandomLoot.Block());
		loot.add(new RandomLoot.Crate());
		loot.add(new RandomLoot.Gem());
		loot.add(new RandomLoot.Ingot());
		loot.add(new RandomLoot.Ore());
	}

	private static void addBaubleLoot() {
		loot.add(new RandomLoot.Amulet());
		loot.add(new RandomLoot.Belt());
		loot.add(new RandomLoot.Ring());
	}

	private static void addMiscLoot() {
		loot.add(new RandomLoot.EnchantedBook());
		loot.add(new RandomLoot.FishingRod());
		loot.add(new RandomLoot.Record());
	}

	public static RandomLoot getRandomLoot() {
		return loot
				.get(MathUtil.getRandomIntegerBetween(0, loot.size() - 1));
	}
}
