package tf2crates.crate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import tconstruct.library.TConstructRegistry;
import tconstruct.library.crafting.ModifyBuilder;
import tconstruct.library.crafting.ToolBuilder;
import tconstruct.library.crafting.ToolRecipe;
import tconstruct.library.modifier.ItemModifier;
import tconstruct.library.tools.ToolCore;
import tconstruct.library.tools.ToolMaterial;
import tconstruct.library.util.IToolPart;
import tlhpoeCore.util.MathUtil;

public class RandomTinkersTool extends RandomLoot {
	private boolean				isLegendary;

	private ToolRecipe			toolType;

	private ArrayList<Integer>	materialIDs;

	public RandomTinkersTool(boolean isLegendary, ToolRecipe toolType) {
		super(null, isLegendary);

		this.isLegendary = isLegendary;
		this.toolType = toolType;
	}

	@Override
	public String getDisplayName() {
		return "A Random " + (isLegendary ? " Legendary" : "")
				+ " Tinker's " + toolType.getType().getLocalizedToolName();
	}

	@Override
	public ItemStack getLoot() {
		if (materialIDs == null) {
			prepareMaterials();
		}

		// determine type
		ToolRecipe recipe = toolType;
		ToolCore type = recipe.getType();

		ItemStack tool = null;
		int tries = 0;
		// try to build the tool
		do {
			tries++;
			// get components
			ItemStack[] parts = new ItemStack[] { null, null, null, null };

			Item[] items = new Item[4];
			items[0] = type.getHeadItem();
			items[1] = type.getHandleItem();
			items[2] = type.getAccessoryItem();
			items[3] = type.getExtraItem();

			for (int i = 0; i < 4; i++) {
				if (items[i] == null) {
					continue;
				}

				do {
					// get a material
					Integer matId = materialIDs
							.get(MathUtil.nextInt(materialIDs.size()));

					parts[i] = new ItemStack(items[i], 1, matId);
				} while (((IToolPart) items[i])
						.getMaterialID(parts[i]) == -1);
			}
			// build the tool
			tool = ToolBuilder.instance.buildTool(parts[0], parts[1],
					parts[2], parts[3], "");
		} while (tool == null && tries < 200);

		if (tool == null) {
			ItemStack oops = new ItemStack(Items.stick);

			oops.setStackDisplayName("Crate Malfunction");

			return oops;
		}

		int modCount = MathUtil.nextInt(3);

		for (int i = modCount; i > 0; i--) {
			doModifyWeapon(tool);
		}

		if (isLegendary) {
			EnchantmentHelper.addRandomEnchantment(MathUtil.getRandom(),
					tool, MathUtil.getRandomIntegerBetween(25, 30));
		}

		return tool;
	}

	private void doModifyWeapon(ItemStack tool) {
		List<ItemModifier> modifiers = ModifyBuilder.instance.itemModifiers;

		ItemModifier mod = modifiers
				.get(MathUtil.nextInt(modifiers.size()));

		ItemStack[] stacks = stackFromModifier(mod);

		if (!mod.matches(stacks, tool)) {
			return;
		}

		mod.addMatchingEffect(tool);

		mod.modify(stacks, tool);
	}

	private static ItemStack[] stackFromModifier(ItemModifier modifier) {
		ItemStack[] stack = new ItemStack[modifier.stacks.size()];
		int i = 0;
		for (Object s : modifier.stacks)
			stack[i++] = (ItemStack) s;

		return stack;
	}

	private void prepareMaterials() {
		materialIDs = new ArrayList<Integer>(); // converted to list for
												// indexed access for
												// random

		for (Map.Entry<Integer,
				ToolMaterial> entry : TConstructRegistry.toolMaterials
						.entrySet()) {
			materialIDs.add(entry.getKey());
		}
	}
}
