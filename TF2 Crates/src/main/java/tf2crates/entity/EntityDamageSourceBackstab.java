package tf2crates.entity;

import net.minecraft.entity.Entity;
import net.minecraft.util.EntityDamageSource;

public class EntityDamageSourceBackstab extends EntityDamageSource {
	private EntityDamageSourceBackstab(Entity attacker) {
		super("backstab", attacker);

		this.setDamageBypassesArmor();
	}

	public static EntityDamageSourceBackstab
			causeBackstabDamage(Entity attacker) {
		return new EntityDamageSourceBackstab(attacker);
	}
}