package tf2crates.handler;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTool;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumChatFormatting;
import net.minecraftforge.event.AnvilUpdateEvent;
import tf2crates.ServerProxyTC;
import tf2crates.crate.RandomLoot;
import tf2crates.item.ItemUnusualEffect;

public class AnvilHandler {
	public static boolean	repairHat, makeUnbreakable;

	public static int		strangeCost, unusualCost, descriptionCost;

	@SubscribeEvent
	public void anvilUpdate(AnvilUpdateEvent event) {
		ItemStack left = event.left;
		ItemStack right = event.right;

		if (left != null && right != null) {
			String leftN = left.getItem().getUnlocalizedName();

			if (left.getItem() instanceof ItemTool
					|| leftN.contains("sword") || leftN.contains("Sword")
					|| leftN.contains("hoe") || leftN.contains("Hoe")) {
				if (left.getItemDamage() > 0) {
					if (right.stackSize == 1
							&& right.getItem() == ServerProxyTC.scrap) {
						ItemStack res = left.copy();

						double amt = 0.1111D;
						int dmg = right.getItemDamage();

						if (dmg == 1) {
							amt = 0.3333D;
						} else if (dmg == 2) {
							amt = 1D;
						}

						res.setItemDamage((int) (res.getItemDamage()
								- (res.getMaxDamage() * amt)));

						event.output = res;
						event.cost = 2;

						return;
					}
				}
			}

			NBTTagCompound lNbt = left.getTagCompound();

			if (lNbt == null) {
				left.setTagCompound(lNbt = new NBTTagCompound());
			}

			if (RandomLoot.WEAPONS.contains(left.getItem())
					&& !lNbt.getBoolean("Golden")) {
				if (right.getItem() == ServerProxyTC.paint
						&& right.getItemDamage() == 11) {
					ItemStack res = left.copy();

					res.getTagCompound().setBoolean("Golden", true);
					res.setItemDamage(0);

					res.setStackDisplayName(EnumChatFormatting.YELLOW
							+ "Strange Minecraftium "
							+ res.getDisplayName());

					event.output = res;
					event.cost = 1;
				}
			}

			if (!lNbt.hasKey("UnusualEffect")) {
				if (left.getItem() instanceof ItemArmor) {
					ItemArmor armor = (ItemArmor) left.getItem();

					if (armor.armorType == 0) {
						if (right.getItem() instanceof ItemUnusualEffect) {
							ItemStack result = left.copy();

							result.getTagCompound().setInteger(
									"UnusualEffect",
									right.getItemDamage());

							if (makeUnbreakable) {
								result.getTagCompound()
										.setBoolean("Unbreakable", true);
							} else if (repairHat) {
								result.setItemDamage(0);
							}

							event.output = result;
							event.cost = unusualCost;
						}

					}
				}
			}

			if (!lNbt.hasKey("Strange") && left.stackSize == 1) {
				if (right.getItem() == ServerProxyTC.strangifier) {
					ItemStack result = left.copy();

					result.getTagCompound().setInteger("Strange", 0);

					event.output = result;
					event.cost = strangeCost;
				}
			}
		}
	}
}