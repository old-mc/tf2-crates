package tf2crates.handler;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import tf2crates.ReferenceTC;
import tf2crates.ServerProxyTC;
import tlhpoeCore.network.MessagePlaySound;

public class AttackHandler {
	public static double marketGardenerMinimumFall;

	@SubscribeEvent
	public void attackHandler(LivingHurtEvent event) {
		Entity attackerE = event.source.getEntity();

		if (attackerE != null && attackerE instanceof EntityLivingBase) {
			EntityLivingBase attacker = (EntityLivingBase) attackerE;
			ItemStack heldItem = attacker.getHeldItem();

			if (heldItem != null && heldItem.getItem() != null) {
				Item item = heldItem.getItem();
				boolean playSound = false;

				if (item == ServerProxyTC.equalizer) {
					float per =
							attacker.getHealth() / attacker.getMaxHealth();

					if (per <= 0.25F) {
						event.ammount += 8;
					} else if (per <= 0.5F) {
						event.ammount += 4;
					} else if (per <= 0.75F) {
						event.ammount += 2;
					}
				} else if (item == ServerProxyTC.marketGardener) {
					if (attacker.fallDistance >= marketGardenerMinimumFall) {
						event.ammount += 15;

						playSound = true;
					}
				} else if (item == ServerProxyTC.axtinguisher) {
					if (event.entityLiving.isBurning()) {
						event.ammount += 8;

						playSound = true;
					}
				} else if (item == ServerProxyTC.sharpenedVolcanoFragment) {
					event.entityLiving.setFire(10);
				}

				if (playSound && attacker instanceof EntityPlayerMP) {
					new MessagePlaySound(
							ReferenceTC.ID + ":tf2crates.crit")
									.sendTo((EntityPlayerMP) attacker);
				}
			}
		}

		ItemStack heldItem = event.entityLiving.getHeldItem();

		if (heldItem != null && heldItem.getItem() != null) {
			Item item = heldItem.getItem();

			if (item == ServerProxyTC.powerJack) {
				event.ammount *= 1.5;
			} else if (item == ServerProxyTC.conniversKunai) {
				event.ammount *= 2;
			}
		}
	}
}