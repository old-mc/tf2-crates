package tf2crates.handler;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import tf2crates.ReferenceTC;
import tf2crates.ServerProxyTC;
import tf2crates.crate.Crates;
import tlhpoeCore.util.MathUtil;

public class DeathHandler {
	public static int[]	crateChance;
	public static int[]	keyChance;

	@SubscribeEvent
	public void mobDeath(LivingDropsEvent event) {
		EntityLivingBase entity = event.entityLiving;

		if (entity instanceof EntityMob) {
			if (MathUtil.getChance(crateChance[0], crateChance[1])) {
				String special = ReferenceTC.CURRENT_SPECIAL_CRATE;

				if (special != null && MathUtil.getChance(1, 5)) {
					event.drops.add(new EntityItem(entity.worldObj,
							entity.posX, entity.posY + 1, entity.posZ,
							new ItemStack(ServerProxyTC.crate, 1,
									Crates.getSeriesFromName(special))));
				} else {
					event.drops.add(new EntityItem(entity.worldObj,
							entity.posX, entity.posY + 1, entity.posZ,
							new ItemStack(ServerProxyTC.crate, 1, MathUtil
									.nextInt(Crates.getNumberOfSeries()
											- Crates.getNumberOfSpecialCrates()))));
				}
			}

			if (MathUtil.getChance(keyChance[0], keyChance[1])) {
				event.drops.add(new EntityItem(entity.worldObj,
						entity.posX, entity.posY + 1, entity.posZ,
						new ItemStack(ServerProxyTC.key, 1)));
			}

			Entity attacker = event.source.getSourceOfDamage();

			if (attacker != null) {
				EntityLivingBase attackerL = null;

				if (attacker instanceof EntityLivingBase) {
					attackerL = (EntityLivingBase) attacker;
				} else if (attacker instanceof EntityArrow) {
					EntityArrow arrow = (EntityArrow) attacker;

					if (arrow.shootingEntity != null
							&& arrow.shootingEntity instanceof EntityLivingBase) {
						attackerL =
								(EntityLivingBase) arrow.shootingEntity;
					}
				}

				if (attackerL != null) {
					ItemStack held = attackerL.getHeldItem();

					if (held != null) {
						NBTTagCompound nbt = held.getTagCompound();

						if (nbt != null && (nbt.hasKey("Strange")
								|| nbt.hasKey("Golden"))) {
							nbt.setInteger("Strange",
									nbt.getInteger("Strange") + 1);
						}

						if (held.getItem() != null && held
								.getItem() == ServerProxyTC.powerJack) {
							attackerL.heal(
									attackerL.getMaxHealth() * 0.25F);
						}
					}
				}
			}
		}
	}
}