package tf2crates.handler;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import cpw.mods.fml.common.gameevent.TickEvent.Phase;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import tlhpoeCore.util.MCUtil;
import tlhpoeCore.util.MathUtil;

public class EventHandler {
	@SubscribeEvent
	public void playerTick(TickEvent.PlayerTickEvent event) {
		EntityPlayer player = event.player;

		if (event.phase == Phase.START) {
			Minecraft mc = MCUtil.getMC();
			if (player != mc.thePlayer || (player == mc.thePlayer
					&& mc.gameSettings.thirdPersonView != 0)) {
				ItemStack helmet = player.getEquipmentInSlot(4);

				if (helmet != null) {
					NBTTagCompound nbt = helmet.getTagCompound();
					if (nbt != null && nbt.hasKey("UnusualEffect")) {
						int unusualEffect =
								nbt.getInteger("UnusualEffect");

						switch (unusualEffect) {
							case (0):
								if (MathUtil.getChance(1, 5)) {
									player.worldObj.spawnParticle("flame",
											player.posX + rDouble(1.25),
											player.posY + 0.5D
													+ rDouble(4),
											player.posZ + rDouble(1.25),
											rDouble(16), rDouble(16),
											rDouble(16));
								}

								break;

							case (1):
								if (MathUtil.getChance(1, 5)) {
									player.worldObj.spawnParticle(
											"explode",
											player.posX + rDouble(1.25),
											player.posY + 0.5D
													+ rDouble(4),
											player.posZ + rDouble(1.25),
											rDouble(8), rDouble(16),
											rDouble(8));
								}

								break;

							case (2):
								if (MathUtil.getChance(1, 5)) {
									player.worldObj.spawnParticle(
											"fireworksSpark",
											player.posX + rDouble(1.25),
											player.posY + 0.5D
													+ rDouble(4),
											player.posZ + rDouble(1.25),
											rDouble(8), rDouble(16),
											rDouble(8));
								}

								break;
							case (3):

								if (MathUtil.getChance(1, 2)) {
									player.worldObj.spawnParticle(
											"depthsuspend",
											player.posX + rDouble(1.25),
											player.posY + 0.5D
													+ rDouble(4),
											player.posZ + rDouble(1.25),
											rDouble(8), rDouble(16),
											rDouble(8));
									player.worldObj.spawnParticle(
											"depthsuspend",
											player.posX + rDouble(1.25),
											player.posY + 0.5D
													+ rDouble(4),
											player.posZ + rDouble(1.25),
											rDouble(8), rDouble(16),
											rDouble(8));
								}

								break;

							case (4):
								if (MathUtil.getChance(1, 4)) {
									player.worldObj.spawnParticle("crit",
											player.posX + rDouble(1.25),
											player.posY + 0.5D
													+ rDouble(4),
											player.posZ + rDouble(1.25),
											rDouble(8), rDouble(16),
											rDouble(8));
									player.worldObj.spawnParticle("crit",
											player.posX + rDouble(1.25),
											player.posY + 0.5D
													+ rDouble(4),
											player.posZ + rDouble(1.25),
											rDouble(8), rDouble(16),
											rDouble(8));
								}

								break;

							case (5):
								if (MathUtil.getChance(1, 4)) {
									player.worldObj.spawnParticle(
											"magicCrit",
											player.posX + rDouble(1.25),
											player.posY + 0.5D
													+ rDouble(4),
											player.posZ + rDouble(1.25),
											rDouble(8), rDouble(16),
											rDouble(8));
									player.worldObj.spawnParticle(
											"magicCrit",
											player.posX + rDouble(1.25),
											player.posY + 0.5D
													+ rDouble(4),
											player.posZ + rDouble(1.25),
											rDouble(8), rDouble(16),
											rDouble(8));
								}

								break;

							case (6):
								if (MathUtil.getChance(1, 5)) {
									player.worldObj.spawnParticle("note",
											player.posX + rDouble(1.25),
											player.posY + 0.5D
													+ rDouble(4),
											player.posZ + rDouble(1.25),
											rDouble(8), rDouble(16),
											rDouble(8));
								}

								break;

							case (7):
								if (MathUtil.getChance(1, 2)) {
									player.worldObj.spawnParticle("portal",
											player.posX + rDouble(4),
											player.posY + 0.5D
													+ rDouble(4),
											player.posZ + rDouble(4),
											rDouble(16), rDouble(16),
											rDouble(16));
									player.worldObj.spawnParticle("portal",
											player.posX + rDouble(4),
											player.posY + 0.5D
													+ rDouble(4),
											player.posZ + rDouble(4),
											rDouble(16), rDouble(16),
											rDouble(16));
								}

								break;

							case (8):
								if (MathUtil.getChance(1, 5)) {
									player.worldObj.spawnParticle(
											"enchantmenttable",
											player.posX + rDouble(1.25),
											player.posY + 0.5D
													+ rDouble(4),
											player.posZ + rDouble(1.25),
											rDouble(2), rDouble(2),
											rDouble(2));
									player.worldObj.spawnParticle(
											"enchantmenttable",
											player.posX + rDouble(1.25),
											player.posY + 0.5D
													+ rDouble(4),
											player.posZ + rDouble(1.25),
											rDouble(2), rDouble(2),
											rDouble(2));
								}

								break;

							case (9):
								if (MathUtil.getChance(1, 5)) {
									player.worldObj.spawnParticle("lava",
											player.posX + rDouble(1.25),
											player.posY + 0.5D
													+ rDouble(4),
											player.posZ + rDouble(1.25),
											rDouble(8), 0, rDouble(8));
								}

								break;

							case (10):
								if (MathUtil.getChance(1, 5)) {
									player.worldObj.spawnParticle(
											"largesmoke",
											player.posX + rDouble(1.25),
											player.posY + 0.5D
													+ rDouble(4),
											player.posZ + rDouble(1.25),
											rDouble(8), rDouble(16),
											rDouble(8));
									player.worldObj.spawnParticle(
											"reddust",
											player.posX + rDouble(1.25),
											player.posY + 0.5D
													+ rDouble(4),
											player.posZ + rDouble(1.25),
											rDouble(8), rDouble(16),
											rDouble(8));
								}

								break;

							case (11):
								if (MathUtil.getChance(1, 5)) {
									player.worldObj.spawnParticle("slime",
											player.posX + rDouble(1.25),
											player.posY + 0.5D
													+ rDouble(4),
											player.posZ + rDouble(1.25),
											rDouble(8), rDouble(16),
											rDouble(8));
									player.worldObj.spawnParticle("slime",
											player.posX + rDouble(1.25),
											player.posY + 0.5D
													+ rDouble(4),
											player.posZ + rDouble(1.25),
											rDouble(8), rDouble(16),
											rDouble(8));
								}

								break;

							case (12):
								if (MathUtil.getChance(1, 5)) {
									player.worldObj.spawnParticle("heart",
											player.posX + rDouble(1.25),
											player.posY + 0.5D
													+ rDouble(4),
											player.posZ + rDouble(1.25),
											rDouble(8), rDouble(16),
											rDouble(8));
								}

								break;

							case (13):
								if (MathUtil.getChance(1, 5)) {
									player.worldObj.spawnParticle(
											MathUtil.getChance(1, 10)
													? "angryVillager"
													: "happyVillager",
											player.posX + rDouble(1),
											player.posY + 0.5D
													+ rDouble(4),
											player.posZ + rDouble(1),
											rDouble(2), rDouble(16),
											rDouble(2));
								}

								break;
						}
					}
				}
			}
		}
	}

	private static double rDouble(double randomness) {
		return (MathUtil.nextDouble() / randomness)
				- (MathUtil.nextDouble() / randomness);
	}
}