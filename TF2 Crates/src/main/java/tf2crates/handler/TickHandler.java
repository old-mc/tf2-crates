package tf2crates.handler;

import java.util.Calendar;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import tf2crates.ReferenceTC;

public class TickHandler {
	private short counter = 0;

	@SubscribeEvent
	public void playerTick(TickEvent.WorldTickEvent event) {
		if (event.phase == TickEvent.Phase.START) {
			if (counter >= 36000) {
				counter = 0;

				Calendar date = Calendar.getInstance();

				int month = date.get(Calendar.MONTH);
				int day = date.getMaximum(Calendar.DAY_OF_MONTH);

				if ((month == Calendar.OCTOBER && day >= 29)
						|| (month == Calendar.NOVEMBER && day < 12)) {
					ReferenceTC.CURRENT_SPECIAL_CRATE = "spookyCrate";
				} else if (month == Calendar.DECEMBER
						&& (day >= 20 && day <= 31)) {
					ReferenceTC.CURRENT_SPECIAL_CRATE = "festiveCrate";
				} else if (month == Calendar.AUGUST
						&& (day >= 24 && day <= 31)) {
					ReferenceTC.CURRENT_SPECIAL_CRATE = "birthdayCrate";
				} else {
					ReferenceTC.CURRENT_SPECIAL_CRATE = null;
				}
			} else {
				counter++;
			}
		}
	}
}