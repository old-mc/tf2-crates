package tf2crates.handler;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.StatCollector;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import tf2crates.item.ItemUnusualEffect;

public class TooltipHandler {
	@SubscribeEvent
	public void toolTip(ItemTooltipEvent event) {
		if (event.itemStack != null) {
			NBTTagCompound nbt = event.itemStack.getTagCompound();

			if (nbt != null) {
				if (nbt.hasKey("UnusualEffect")) {
					event.toolTip.add("Unusual Effect: "
							+ EnumChatFormatting.LIGHT_PURPLE
							+ StatCollector
									.translateToLocal("unusualEffect."
											+ ItemUnusualEffect.TYPES[nbt
													.getInteger(
															"UnusualEffect")]
											+ ".name"));
				}

				if (nbt.hasKey("Strange") || nbt.hasKey("Golden")) {
					event.toolTip.add("# of Mobs Killed: "
							+ EnumChatFormatting.YELLOW
							+ nbt.getInteger("Strange"));
				}

				if (nbt.hasKey("Description")) {
					event.toolTip.add(nbt.getString("Description"));
				}
			}
		}
	}
}