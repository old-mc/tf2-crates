package tf2crates.item;

import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.IIcon;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;
import tf2crates.ReferenceTC;
import tf2crates.ServerProxyTC;
import tf2crates.crate.Crates;
import tf2crates.crate.RandomLoot;
import tlhpoeCore.network.MessagePlaySound;
import tlhpoeCore.util.MathUtil;

public class ItemCrate extends Item {
	public static IIcon[]	sCrateTextures;
	public static int[]		unusualChance;
	public static int[]		weaponChance;

	public ItemCrate() {
		super();

		this.setUnlocalizedName("crate");
		this.setTextureName(ReferenceTC.ID + ":crate");
		this.setMaxStackSize(1);

		this.hasSubtypes = true;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister register) {
		super.registerIcons(register);

		sCrateTextures = new IIcon[Crates.getNumberOfSeries() + 1];

		for (int i = 0; i < sCrateTextures.length; i++) {
			if (Crates.isSeriesSpecial(i)) {
				sCrateTextures[i] = register.registerIcon(ReferenceTC.ID
						+ ":" + Crates.getNameFromSeries(i));
			}
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIconIndex(ItemStack itemStack) {
		int series = 1 + itemStack.getItemDamage();

		if (Crates.isSeriesSpecial(series)) {
			return sCrateTextures[series];
		}

		return super.getIconIndex(itemStack);
	}

	@Override
	public IIcon getIcon(ItemStack itemStack, int pass) {
		int series = 1 + itemStack.getItemDamage();

		if (Crates.isSeriesSpecial(series)) {
			return sCrateTextures[series];
		}

		return super.getIcon(itemStack, pass);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void getSubItems(Item item, CreativeTabs tabs, List list) {
		for (int i = 0; i < Crates.getNumberOfSeries(); i++) {
			list.add(new ItemStack(item, 1, i));
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack itemStack, EntityPlayer player,
			List info, boolean f) {
		int series = itemStack.getItemDamage() + 1;

		info.add(Crates.isSeriesSpecial(series)
				? (EnumChatFormatting.GREEN
						+ (StatCollector.translateToLocal(
								"crate." + Crates.getNameFromSeries(series)
										+ ".name")))
				: EnumChatFormatting.YELLOW + "Crate Series #" + series);

		Object[] lootItems = Crates.getLootForCrate(series);

		for (int i = 0; i < lootItems.length; i++) {
			Object loot = lootItems[i];
			String name = null;

			if (loot instanceof ItemStack) {
				name = ((ItemStack) loot).getDisplayName();
			} else if (loot instanceof RandomLoot) {
				name = ((RandomLoot) loot).getDisplayName();
			}

			info.add(EnumChatFormatting.GRAY + name);
		}

		info.add(EnumChatFormatting.AQUA
				+ "or an Exceedingly Rare Special Item!");
	}

	@Override
	public ItemStack onItemRightClick(ItemStack itemStack, World world,
			EntityPlayer player) {
		if (!world.isRemote) {
			if (player.capabilities.isCreativeMode || player.inventory
					.consumeInventoryItem(ServerProxyTC.key)) {
				int series = itemStack.getItemDamage() + 1;
				Object[] lootItems = Crates.getLootForCrate(series);

				ItemStack stackLoot = null;

				if (MathUtil.getChance(unusualChance[0],
						unusualChance[1])) {
					stackLoot = new ItemStack(ServerProxyTC.unusualEffect,
							1, MathUtil.nextInt(
									ItemUnusualEffect.TYPES.length));
				} else if (MathUtil.getChance(weaponChance[0],
						weaponChance[1])) {
					stackLoot = new ItemStack(
							RandomLoot.WEAPONS.get(MathUtil
									.nextInt(RandomLoot.WEAPONS.size())),
							1);
				} else {
					Object loot =
							lootItems[MathUtil.nextInt(lootItems.length)];

					if (loot instanceof ItemStack) {
						stackLoot = (ItemStack) loot;
					} else if (loot instanceof RandomLoot) {
						stackLoot = ((RandomLoot) loot).getLoot();
					}
				}

				boolean full = true;

				for (int i = 0; i < player.inventory.getSizeInventory();
						i++) {
					if (player.inventory.getStackInSlot(i) == null) {
						full = false;
					}
				}

				if (!full) {
					player.inventory
							.addItemStackToInventory(stackLoot.copy());
				} else {
					player.entityDropItem(stackLoot.copy(), 1F);
				}

				MinecraftServer.getServer().getConfigurationManager()
						.sendChatMsg(new ChatComponentText(player
								.getDisplayName()
								+ " just uncrated "
								+ (stackLoot.stackSize > 1
										? (EnumChatFormatting.AQUA + ""
												+ stackLoot.stackSize
												+ " ")
										: "a ")
								+ EnumChatFormatting.YELLOW
								+ stackLoot.getDisplayName()
								+ (stackLoot.stackSize > 1 ? "s" : "")
								+ EnumChatFormatting.WHITE + "!!!"));

				new MessagePlaySound(ReferenceTC.ID + ":tf2crates.uncrate")
						.sendTo((EntityPlayerMP) player);

				itemStack.stackSize--;
			} else {
				player.addChatMessage(new ChatComponentText("You need a "
						+ EnumChatFormatting.YELLOW
						+ "Steve Co. Supply Crate Key"
						+ EnumChatFormatting.WHITE + " to open this!"));
			}
		}

		return itemStack;
	}
}