package tf2crates.item;

import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import tf2crates.ReferenceTC;

public class ItemKey extends Item {
	public ItemKey() {
		super();

		this.setUnlocalizedName("key");
		this.setTextureName(ReferenceTC.ID + ":key");
		this.setMaxStackSize(1);
	}

	@Override
	public EnumRarity getRarity(ItemStack itemStack) {
		return EnumRarity.rare;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack itemStack, EntityPlayer player,
			List info, boolean f) {
		info.add(EnumChatFormatting.YELLOW
				+ "Used to open locked supply crates.");
	}
}