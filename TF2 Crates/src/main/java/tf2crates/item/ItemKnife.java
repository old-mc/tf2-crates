package tf2crates.item;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.IIcon;
import tf2crates.ReferenceTC;
import tf2crates.ServerProxyTC;
import tf2crates.crate.RandomLoot;
import tf2crates.entity.EntityDamageSourceBackstab;
import tlhpoeCore.network.MessagePlaySound;
import tlhpoeCore.util.MiscUtil;

public class ItemKnife extends ItemSword {
	public static double	backstabDifficulty	= 0.75D;

	public IIcon			goldTexture;

	public ItemKnife(String name) {
		super(ServerProxyTC.MOD_WEAPON);

		this.setUnlocalizedName(name);
		this.setTextureName(ReferenceTC.ID + ":weapon/" + name);

		RandomLoot.WEAPONS.add(this);
	}

	public ItemKnife(String name, float dmg) {
		this(name);

		MiscUtil.replaceField("field_150934_a", ItemSword.class, dmg,
				this);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister register) {
		super.registerIcons(register);

		this.goldTexture =
				register.registerIcon(ReferenceTC.ID + ":weapon/gold/"
						+ this.getUnlocalizedName().substring(5));
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIconIndex(ItemStack itemStack) {
		NBTTagCompound nbt = itemStack.getTagCompound();

		if (nbt != null && nbt.getBoolean("Golden")) {
			return this.goldTexture;
		}

		return super.getIconIndex(itemStack);
	}

	@Override
	public IIcon getIcon(ItemStack itemStack, int pass) {
		NBTTagCompound nbt = itemStack.getTagCompound();

		if (nbt != null && nbt.getBoolean("Golden")) {
			return this.goldTexture;
		}

		return super.getIcon(itemStack, pass);
	}

	@Override
	public boolean hitEntity(ItemStack itemStack, EntityLivingBase prey,
			EntityLivingBase attacker) {
		itemStack.damageItem(1, attacker);

		float pYaw = prey.rotationYawHead % 360F;

		pYaw = pYaw < 0 ? 360 + pYaw : prey.rotationYawHead;

		float aYaw = attacker.rotationYawHead < 0
				? 360 + attacker.rotationYawHead
				: attacker.rotationYawHead;

		float newRot = pYaw - aYaw;

		if (newRot < (90 * backstabDifficulty)
				&& newRot > (-90 * backstabDifficulty)) {
			if (!attacker.worldObj.isRemote) {
				new MessagePlaySound(ReferenceTC.ID + ":tf2crates.crit")
						.sendTo((EntityPlayerMP) attacker);
			}

			if (itemStack.getItem() == ServerProxyTC.conniversKunai) {
				attacker.heal(prey.getMaxHealth());
			}

			prey.attackEntityFrom(EntityDamageSourceBackstab
					.causeBackstabDamage(attacker),
					prey.getMaxHealth() * 600);
		}

		return false;
	}
}