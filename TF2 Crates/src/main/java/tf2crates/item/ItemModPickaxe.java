package tf2crates.item;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import tf2crates.ReferenceTC;
import tf2crates.ServerProxyTC;
import tf2crates.crate.RandomLoot;

public class ItemModPickaxe extends ItemPickaxe {
	public IIcon goldTexture;

	public ItemModPickaxe(String name) {
		super(ServerProxyTC.MOD_WEAPON);

		this.setUnlocalizedName(name);
		this.setTextureName(ReferenceTC.ID + ":weapon/" + name);

		RandomLoot.WEAPONS.add(this);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister register) {
		super.registerIcons(register);

		this.goldTexture =
				register.registerIcon(ReferenceTC.ID + ":weapon/gold/"
						+ this.getUnlocalizedName().substring(5));
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIconIndex(ItemStack itemStack) {
		NBTTagCompound nbt = itemStack.getTagCompound();

		if (nbt != null && nbt.getBoolean("Golden")) {
			return this.goldTexture;
		}

		return super.getIconIndex(itemStack);
	}

	@Override
	public IIcon getIcon(ItemStack itemStack, int pass) {
		NBTTagCompound nbt = itemStack.getTagCompound();

		if (nbt != null && nbt.getBoolean("Golden")) {
			return this.goldTexture;
		}

		return super.getIcon(itemStack, pass);
	}

	@Override
	public void onUpdate(ItemStack itemStack, World world, Entity entity,
			int f, boolean f2) {
		if (this == ServerProxyTC.escapePlan) {
			if (entity instanceof EntityLivingBase) {
				EntityLivingBase living = (EntityLivingBase) entity;
				ItemStack heldItem = living.getHeldItem();

				if (heldItem != null && heldItem.getItem() != null
						&& heldItem
								.getItem() == ServerProxyTC.escapePlan) {
					float per = living.getHealth() / living.getMaxHealth();
					int amplifier = -1;

					if (per <= 0.25F) {
						amplifier = 2;
					} else if (per <= 0.5F) {
						amplifier = 1;
					} else if (per <= 0.75F) {
						amplifier = 0;
					}

					if (amplifier != -1) {
						living.addPotionEffect(new PotionEffect(
								Potion.moveSpeed.id, 2, amplifier, true));
					}

					if (amplifier > 0) {
						living.addPotionEffect(
								new PotionEffect(Potion.digSpeed.id, 2,
										amplifier - 1, true));
					}
				}
			}
		}
	}
}