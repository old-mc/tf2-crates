package tf2crates.item;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.ItemSpade;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTool;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.IIcon;
import tf2crates.ReferenceTC;
import tf2crates.ServerProxyTC;
import tf2crates.crate.RandomLoot;
import tlhpoeCore.util.MiscUtil;

public class ItemModShovel extends ItemSpade {
	public IIcon goldTexture;

	public ItemModShovel(String name) {
		super(ServerProxyTC.MOD_WEAPON);

		this.setUnlocalizedName(name);
		this.setTextureName(ReferenceTC.ID + ":weapon/" + name);

		RandomLoot.WEAPONS.add(this);
	}

	public ItemModShovel(String name, float dmg) {
		this(name);

		MiscUtil.replaceField("damageVsEntity", ItemTool.class, dmg, this);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister register) {
		super.registerIcons(register);

		this.goldTexture =
				register.registerIcon(ReferenceTC.ID + ":weapon/gold/"
						+ this.getUnlocalizedName().substring(5));
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIconIndex(ItemStack itemStack) {
		NBTTagCompound nbt = itemStack.getTagCompound();

		if (nbt != null && nbt.getBoolean("Golden")) {
			return this.goldTexture;
		}

		return super.getIconIndex(itemStack);
	}

	@Override
	public IIcon getIcon(ItemStack itemStack, int pass) {
		NBTTagCompound nbt = itemStack.getTagCompound();

		if (nbt != null && nbt.getBoolean("Golden")) {
			return this.goldTexture;
		}

		return super.getIcon(itemStack, pass);
	}
}