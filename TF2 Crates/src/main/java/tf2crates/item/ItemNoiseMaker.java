package tf2crates.item;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import tf2crates.ReferenceTC;
import tlhpoeCore.util.MathUtil;

public class ItemNoiseMaker extends Item {
	public String[] noises;

	public ItemNoiseMaker(String name, String... noises) {
		super();

		this.setUnlocalizedName(name);
		this.setTextureName(ReferenceTC.ID + ":" + name);
		this.setMaxStackSize(1);

		this.noises = noises;
	}

	@Override
	public void onPlayerStoppedUsing(ItemStack itemStack, World world,
			EntityPlayer player, int time) {
		if (this.getMaxItemUseDuration(itemStack) - time > 10) {
			player.playSound(noises[MathUtil.nextInt(noises.length)],
					0.25F + ((float) (MathUtil.nextDouble() / 8)), 1F);
		}
	}

	@Override
	public ItemStack onItemRightClick(ItemStack itemStack, World world,
			EntityPlayer player) {
		player.setItemInUse(itemStack,
				this.getMaxItemUseDuration(itemStack));
		return itemStack;
	}

	@Override
	public int getMaxItemUseDuration(ItemStack itemStack) {
		return Integer.MAX_VALUE;
	}

	@Override
	public EnumAction getItemUseAction(ItemStack itemStack) {
		return EnumAction.block;
	}
}