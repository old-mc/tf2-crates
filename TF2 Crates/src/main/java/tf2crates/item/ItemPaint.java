package tf2crates.item;

import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;
import tf2crates.ReferenceTC;

public class ItemPaint extends Item { // make paint able to change the
										// color of wool blocks
	public static final String[]	TYPES	= { "black", "red", "green",
			"brown", "blue", "purple", "cyan", "silver", "gray", "pink",
			"lime", "yellow", "lightBlue", "magenta", "orange", "white" };
	public IIcon[]					textures;

	public ItemPaint() {
		super();

		this.setUnlocalizedName("paint");
		this.setMaxStackSize(1);

		this.hasSubtypes = true;

		this.setContainerItem(this);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister) {
		textures = new IIcon[TYPES.length];

		for (int i = 0; i < TYPES.length; i++) {
			textures[i] = iconRegister
					.registerIcon(ReferenceTC.ID + ":paint/" + TYPES[i]);
		}
	}

	@Override
	public String getItemStackDisplayName(ItemStack itemStack) {
		return StatCollector.translateToLocal(
				"paint." + TYPES[itemStack.getItemDamage()] + ".name");
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void getSubItems(Item item, CreativeTabs tabs, List list) {
		for (int i = 0; i < TYPES.length; i++) {
			list.add(new ItemStack(item, 1, i));
		}
	}

	@Override
	public EnumRarity getRarity(ItemStack itemStack) {
		return EnumRarity.rare;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIconFromDamage(int dmg) {
		return textures[dmg];
	}

	@Override
	public boolean
			doesContainerItemLeaveCraftingGrid(ItemStack itemStack) {
		return false;
	}

	@Override
	public boolean hasContainerItem(ItemStack stack) {
		return true;
	}

	@Override
	public ItemStack getContainerItem(ItemStack itemStack) {
		return itemStack;
	}

	@Override
	public boolean onItemUse(ItemStack itemStack, EntityPlayer player,
			World world, int x, int y, int z, int meta, float p_77648_8_,
			float p_77648_9_, float p_77648_10_) {
		Block block = world.getBlock(x, y, z);

		if (block == Blocks.wool || block == Blocks.carpet
				|| block == Blocks.glass || block == Blocks.stained_glass
				|| block == Blocks.glass_pane
				|| block == Blocks.stained_glass_pane
				|| block == Blocks.hardened_clay
				|| block == Blocks.stained_hardened_clay) {
			if (!world.isRemote) {
				if (block == Blocks.glass) {
					block = Blocks.stained_glass;
				}

				if (block == Blocks.glass_pane) {
					block = Blocks.stained_glass_pane;
				}

				if (block == Blocks.hardened_clay) {
					block = Blocks.stained_hardened_clay;
				}

				world.setBlock(x, y, z, block,
						15 - itemStack.getItemDamage(), 2);
			}

			return true;
		}

		return false;
	}
}