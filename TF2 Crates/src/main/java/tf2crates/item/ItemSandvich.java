package tf2crates.item;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;
import tf2crates.ReferenceTC;
import tf2crates.crate.RandomLoot;

public class ItemSandvich extends ItemFood {
	public ItemSandvich() {
		super(20, 0.32F, true);

		this.setUnlocalizedName("sandvich");
		this.setTextureName(ReferenceTC.ID + ":sandvich");
		this.setMaxStackSize(1);
		this.setMaxDamage(6001);
		this.setAlwaysEdible();

		RandomLoot.WEAPONS.add(this);
	}

	@Override
	protected void onFoodEaten(ItemStack itemStack, World world,
			EntityPlayer player) {
		if (!world.isRemote) {
			player.addPotionEffect(
					new PotionEffect(Potion.regeneration.id, 600, 2));
			player.addPotionEffect(
					new PotionEffect(Potion.digSpeed.id, 600, 2));
			player.addPotionEffect(
					new PotionEffect(Potion.moveSpeed.id, 600, 1));

			player.heal(player.getMaxHealth());

			itemStack.setItemDamage(6000);
		}
	}

	@Override
	public ItemStack onEaten(ItemStack itemStack, World world,
			EntityPlayer player) {
		player.getFoodStats().func_151686_a(this, itemStack);

		world.playSoundAtEntity(player, "random.burp", 0.5F,
				world.rand.nextFloat() * 0.1F + 0.9F);

		this.onFoodEaten(itemStack, world, player);

		return itemStack;
	}

	@Override
	public ItemStack onItemRightClick(ItemStack itemStack, World world,
			EntityPlayer player) {
		if (itemStack.getItemDamage() == 0) {
			player.setItemInUse(itemStack,
					this.getMaxItemUseDuration(itemStack));
		}

		return itemStack;
	}

	@Override
	public void onUpdate(ItemStack itemStack, World world, Entity entity,
			int f, boolean f2) {
		if (!world.isRemote) {
			int dmg = itemStack.getItemDamage();

			if (dmg > 0) {
				itemStack.setItemDamage(dmg - 1);
			}
		}
	}
}