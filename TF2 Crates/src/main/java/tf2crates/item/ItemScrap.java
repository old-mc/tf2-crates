package tf2crates.item;

import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.StatCollector;
import tf2crates.ReferenceTC;

public class ItemScrap extends Item {
	public static final String[]		TYPES		=
			{ "scrap", "reclaimed", "refined" };
	public static final EnumRarity[]	RARITIES	=
			{ EnumRarity.uncommon, EnumRarity.rare, EnumRarity.epic };
	public IIcon[]						textures;

	public ItemScrap() {
		super();

		this.setUnlocalizedName("scrap");

		this.hasSubtypes = true;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconRegister) {
		textures = new IIcon[TYPES.length];

		for (int i = 0; i < TYPES.length; i++) {
			textures[i] = iconRegister
					.registerIcon(ReferenceTC.ID + ":" + TYPES[i]);
		}
	}

	@Override
	public String getItemStackDisplayName(ItemStack itemStack) {
		return StatCollector.translateToLocal(
				"item." + TYPES[itemStack.getItemDamage()] + ".name");
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void getSubItems(Item item, CreativeTabs tabs, List list) {
		for (int i = 0; i < TYPES.length; i++) {
			list.add(new ItemStack(item, 1, i));
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIconFromDamage(int dmg) {
		return textures[dmg];
	}

	@Override
	public EnumRarity getRarity(ItemStack itemStack) {
		return RARITIES[itemStack.getItemDamage()];
	}
}