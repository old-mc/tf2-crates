package tf2crates.item;

import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.StatCollector;
import tf2crates.ReferenceTC;

public class ItemUnusualEffect extends Item {
	public static final String[] TYPES = { "burningFlames", "poof",
			"whiteSparkles", "flies", "critical", "magicalCrit", "musical",
			"end", "enchanted", "volcano", "smoking", "slime", "loving",
			"villager" };

	public ItemUnusualEffect() {
		super();

		this.setUnlocalizedName("unusualEffect");
		this.setTextureName(ReferenceTC.ID + ":unusualEffect");
		this.setMaxStackSize(1);
		this.setFull3D();

		this.hasSubtypes = true;
	}

	@Override
	public String getItemStackDisplayName(ItemStack itemStack) {
		return StatCollector
				.translateToLocal("unusualEffect."
						+ TYPES[itemStack.getItemDamage()] + ".name")
				+ " Unusual Effect";
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void getSubItems(Item item, CreativeTabs tabs, List list) {
		for (int i = 0; i < TYPES.length; i++) {
			list.add(new ItemStack(item, 1, i));
		}
	}

	@Override
	public EnumRarity getRarity(ItemStack itemStack) {
		return EnumRarity.epic;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack itemStack, EntityPlayer player,
			List info, boolean f) {
		info.add(EnumChatFormatting.YELLOW
				+ "Combine this with the helmet of your choice!");
	}
}