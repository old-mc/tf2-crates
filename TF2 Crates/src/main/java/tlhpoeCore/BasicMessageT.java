package tlhpoeCore;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;

public abstract class BasicMessageT implements IMessage {
	@Override
	public abstract void fromBytes(ByteBuf buf);

	@Override
	public abstract void toBytes(ByteBuf buf);

	public void sendToAll() {
		TLHPoE.networkChannel.sendToAll(this);
	}

	public void sendTo(EntityPlayerMP player) {
		TLHPoE.networkChannel.sendTo(this, player);
	}

	public void sendToServer() {
		TLHPoE.networkChannel.sendToServer(this);
	}
}