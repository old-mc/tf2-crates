package tlhpoeCore;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.network.FMLNetworkEvent.ClientConnectedToServerEvent;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ChatComponentText;
import tlhpoeCore.util.MCUtil;

public class ClientProxyT extends ServerProxyT {
	@Override
	public void doClient() {
		FMLCommonHandler.instance().bus().register(this);

		TLHPoE.registerUpdateDetector(ReferenceT.ID, ReferenceT.NAME,
				ReferenceT.VERSION, "0B6mhkrh-GwwwTFkyWWh5b3BiYU0");
	}

	@SubscribeEvent
	public void connect(ClientConnectedToServerEvent event) {
		if (!Loader.isModLoaded("VersionChecker")) {
			new Thread(new Runnable() {
				@Override
				public void run() {
					boolean done = false;

					while (!done) {
						EntityPlayer player = MCUtil.getMC().thePlayer;

						if (player != null) {
							done = true;

							player.addChatMessage(new ChatComponentText(
									"If you want to be notified of the latest update from any of TLHPoE's mods, install the VersionChecker mod!"));
						} else {
							try {
								Thread.sleep(10);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
					}

					Thread.currentThread().interrupt();
				}
			}).start();
		}
	}
}