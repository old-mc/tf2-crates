package tlhpoeCore;

public class ReferenceT {
	public static final String	ID				= "tlhpoeCore";
	public static final String	NAME			= "TLHPoE Core";
	public static final String	VERSION			= "1.9";

	public static boolean		DEOBFUSCATED	= false;

	public static final int		DEFAULT_WIDTH	= 854;
	public static final int		DEFAULT_HEIGHT	= 480;
}