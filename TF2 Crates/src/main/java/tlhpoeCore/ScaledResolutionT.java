package tlhpoeCore;

import net.minecraft.client.gui.ScaledResolution;
import tlhpoeCore.util.MCUtil;

public class ScaledResolutionT extends ScaledResolution {
	public ScaledResolutionT(int width, int height) {
		super(MCUtil.getMC(), width, height);
	}

	public double getScaledX(double x) {
		return (x / ReferenceT.DEFAULT_WIDTH) * getScaledWidth_double();
	}

	public double getScaledY(double y) {
		return (y / ReferenceT.DEFAULT_HEIGHT) * getScaledHeight_double();
	}
}