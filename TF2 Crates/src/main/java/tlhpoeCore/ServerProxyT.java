package tlhpoeCore;

import cpw.mods.fml.relauncher.Side;
import tlhpoeCore.network.MessagePlaySound;

public class ServerProxyT {
	public void doServer() {
		TLHPoE.networkChannel.registerMessage(
				MessagePlaySound.Handler.class, MessagePlaySound.class,
				TLHPoE.getNextMessageID(), Side.CLIENT);
	}

	public void doClient() {
	}
}