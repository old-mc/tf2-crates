package tlhpoeCore;

import static tlhpoeCore.ReferenceT.ID;
import static tlhpoeCore.ReferenceT.NAME;
import static tlhpoeCore.ReferenceT.VERSION;

import java.io.IOException;
import java.net.MalformedURLException;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInterModComms;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraft.launchwrapper.Launch;
import net.minecraft.nbt.NBTTagCompound;
import tlhpoeCore.util.MiscUtil;

@Mod(modid = ID, name = NAME, version = VERSION)
public class TLHPoE {
	@Instance(ID)
	public static TLHPoE				instance;
	@SidedProxy(clientSide = ID + ".ClientProxyT",
			serverSide = ID + ".ServerProxyT")
	public static ServerProxyT			proxy;

	public static SimpleNetworkWrapper	networkChannel;

	private static int					nextMessageID	= 0;

	@EventHandler
	public void preInit(FMLPreInitializationEvent e) {
		ReferenceT.DEOBFUSCATED = (Boolean) Launch.blackboard
				.get("fml.deobfuscatedEnvironment");

		networkChannel =
				NetworkRegistry.INSTANCE.newSimpleChannel("TLHPoE");

		proxy.doServer();
		proxy.doClient();
	}

	public static void registerUpdateDetector(String modid, String name,
			String modVersion, String driveID) {
		String newVersion = null;

		try {
			newVersion = MiscUtil
					.getURLText("https://docs.google.com/uc?authuser=0&id="
							+ driveID + "&export=download");
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return;
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}

		if (modVersion.equals(newVersion)) {
			return;
		}

		NBTTagCompound nbt = new NBTTagCompound();

		nbt.setString("modDisplayName", name);
		nbt.setString("oldVersion", modVersion);
		nbt.setString("newVersion", newVersion);
		nbt.setString("updateUrl", "http://adfoc.us/23774349240713");
		nbt.setBoolean("isDirectLink", false);

		FMLInterModComms.sendRuntimeMessage(modid, "VersionChecker",
				"addUpdate", nbt);
	}

	public static int getNextMessageID() {
		return nextMessageID++;
	}
}