package tlhpoeCore.network;

import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import tlhpoeCore.BasicMessageT;
import tlhpoeCore.util.MCUtil;

public class MessagePlaySound extends BasicMessageT {
	public String soundName;

	public MessagePlaySound() {
		this("");
	}

	public MessagePlaySound(String soundName) {
		this.soundName = soundName;
	}

	@Override
	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeUTF8String(buf, soundName);
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		soundName = ByteBufUtils.readUTF8String(buf);
	}

	public static class Handler
			implements IMessageHandler<MessagePlaySound, IMessage> {
		@Override
		public IMessage onMessage(MessagePlaySound message,
				MessageContext ctx) {
			MCUtil.getMC().thePlayer.playSound(message.soundName, 0.25F,
					1F);

			return null;
		}
	}
}