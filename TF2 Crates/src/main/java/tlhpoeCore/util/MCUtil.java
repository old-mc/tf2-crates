package tlhpoeCore.util;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import tlhpoeCore.ReferenceT;

public class MCUtil {
	public static Minecraft getMC() {
		return Minecraft.getMinecraft();
	}

	public static Item getRandomItemOrBlock() {
		Item i = null;

		int length = Item.itemRegistry.getKeys().toArray().length;

		Object select =
				Item.itemRegistry.getObjectById(MathUtil.nextInt(length));

		if (select != null && select instanceof Item) {
			i = (Item) select;
		} else {
			return getRandomItemOrBlock();
		}

		return i;
	}

	public static Item getRandomItem() {
		Item i = null;

		int length = Item.itemRegistry.getKeys().toArray().length;

		Object select =
				Item.itemRegistry.getObjectById(MathUtil.nextInt(length));

		if (select != null && select instanceof Item
				&& !(select instanceof ItemBlock)) {
			i = (Item) select;
		} else {
			return getRandomItem();
		}

		return i;
	}

	public static Item getRandomBlock() {
		Item i = null;

		int length = Item.itemRegistry.getKeys().toArray().length;

		Object select =
				Item.itemRegistry.getObjectById(MathUtil.nextInt(length));

		if (select != null && select instanceof ItemBlock) {
			i = (Item) select;
		} else {
			return getRandomBlock();
		}

		return i;
	}

	public static NBTTagCompound
			getPlayerNBTCompound(EntityPlayer player) {
		NBTTagCompound nbt = player.getEntityData();

		if (nbt.getCompoundTag(ReferenceT.NAME) == null) {
			nbt.setTag(ReferenceT.NAME, new NBTTagCompound());
		}

		return nbt.getCompoundTag(ReferenceT.NAME);
	}

	public static ResourceLocation newResource(String url) {
		return new ResourceLocation(url);
	}
}