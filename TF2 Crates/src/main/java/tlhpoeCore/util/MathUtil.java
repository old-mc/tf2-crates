package tlhpoeCore.util;

import java.util.Random;

public class MathUtil {
	private static final Random RANDOM = new Random();

	public static int nextInt(int n) {
		return RANDOM.nextInt(n);
	}

	public static double nextDouble() {
		return RANDOM.nextDouble();
	}

	public static int getRandomIntegerBetween(Random r, int min, int max) {
		return r.nextInt(max - min + 1) + min;
	}

	public static int getRandomIntegerBetween(int min, int max) {
		return nextInt(max - min + 1) + min;
	}

	public static boolean getChance(Random r, int chance, int range) {
		return chance >= range ? true : r.nextInt(range - 1) <= chance - 1;
	}

	public static boolean getChance(int chance, int range) {
		return chance >= range ? true : nextInt(range - 1) <= chance - 1;
	}

	public static Random getRandom() {
		return RANDOM;
	}
}