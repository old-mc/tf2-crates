package tlhpoeCore.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.net.URL;

public class MiscUtil {
	public static Field getField(String fieldName, Class<?> clazz) {
		Field field = null;

		try {
			field = clazz.getDeclaredField(fieldName);
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}

		return field;
	}

	public static Object getFieldValue(String fieldName, Class<?> clazz,
			Object instance) {
		Field field = getField(fieldName, clazz);
		makePublic(field);

		try {
			return field.get(instance);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		return null;
	}

	public static void replaceField(String fieldName, Class<?> clazz,
			Object newValue, Object instance) {
		Field field = getField(fieldName, clazz);

		if (field != null) {
			makePublic(field);

			try {
				field.set(instance, newValue);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}

	private static void makePublic(Field field) {
		field.setAccessible(true);

		Field modField = null;

		try {
			modField = Field.class.getDeclaredField("modifiers");
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}

		modField.setAccessible(true);

		try {
			modField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	public static String getURLText(String url)
			throws MalformedURLException, IOException {
		BufferedReader versionFile = new BufferedReader(
				new InputStreamReader(new URL(url).openStream()));

		String s = versionFile.readLine();

		versionFile.close();

		return s;
	}
}