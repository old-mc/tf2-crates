package tlhpoeCore.util;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;

public class RenderUtil {
	public static void drawString(String msg, int x, int y, int color) {
		MCUtil.getMC().fontRenderer.drawString(msg, x, y, color);
	}

	public static void drawOutlinedString(String msg, int x, int y,
			int color, int outlineColor) {
		Minecraft mc = MCUtil.getMC();

		mc.fontRenderer.drawString(msg, x - 1, y, outlineColor);
		mc.fontRenderer.drawString(msg, x + 1, y, outlineColor);
		mc.fontRenderer.drawString(msg, x, y - 1, outlineColor);
		mc.fontRenderer.drawString(msg, x, y + 1, outlineColor);

		drawString(msg, x, y, color);
	}

	public static void drawTexturedQuad(int x, int y, int width,
			int height, int u, int v, int uSize, int vSize, int texSizeX,
			int texSizeY, float zLevel) {
		float uFact = 1f / texSizeX;
		float vFact = 1f / texSizeY;

		int uEnd = u + uSize;
		int vEnd = v + vSize;

		Tessellator t = Tessellator.instance;

		t.startDrawingQuads();
		t.addVertexWithUV(x, y + height, zLevel, u * uFact, vEnd * vFact);
		t.addVertexWithUV(x + width, y + height, zLevel, uEnd * uFact,
				vEnd * vFact);
		t.addVertexWithUV(x + width, y, zLevel, uEnd * uFact, v * vFact);
		t.addVertexWithUV(x, y, zLevel, u * uFact, v * vFact);

		t.draw();
	}
}