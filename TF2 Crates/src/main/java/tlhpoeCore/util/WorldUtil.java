package tlhpoeCore.util;

import net.minecraft.block.Block;
import net.minecraft.world.World;

public class WorldUtil {
	public static int getTopBlock(World world, int x, int z) {
		for (int y = 0; y < 256; y++) {
			if (world.canBlockSeeTheSky(x, y, z)) {
				return y;
			}
		}

		return -1;
	}

	public static int getTopBlockOfType(World world, int x, int z,
			Block block) {
		for (int y = 0; y < 256; y++) {
			if (world.getBlock(x, 256 - y, z) == block) {
				return 256 - y;
			}
		}

		return -1;
	}

	public static void fillCircle(World world, double x, double y,
			double z, int radius, Block block) {
		world.setBlock((int) x, (int) y, (int) z, block);

		world.setBlock((int) x + 1, (int) y, (int) z, block);
		world.setBlock((int) x - 1, (int) y, (int) z, block);

		world.setBlock((int) x, (int) y, (int) z + 1, block);
		world.setBlock((int) x, (int) y, (int) z - 1, block);

		x += 0.5;
		y += 0.5;
		z += 0.5;

		while (radius != 0) {
			for (int i = 0; i < 360; i++) {
				world.setBlock((int) (x + radius * Math.cos(i)), (int) y,
						(int) (z + radius * Math.sin(i)), block);
			}

			radius--;
		}
	}

	public static void fillRect(World world, Block filler, int x, int y,
			int z, int width, int length, int height) {
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < length; j++) {
				for (int k = 0; k < height; k++) {
					world.setBlock(x + i, y + k, z + j, filler);
				}
			}
		}
	}
}